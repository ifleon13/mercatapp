package com.fleon.mercatapp.modules.login.views


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.database.FirebaseDatabase
import java.util.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.utils.AppConstants
import com.fleon.mercatapp.commons.database.user.UserDB
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.databinding.FragmentLoginBinding
import com.fleon.mercatapp.modules.login.models.UserFB
import com.fleon.mercatapp.modules.login.viewmodels.LoginViewModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import java.security.MessageDigest


/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {


    private var firebaseDatabase: FirebaseDatabase? = null
    private var firebaseFirestore: FirebaseFirestore? = null
    private lateinit var binding: FragmentLoginBinding
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //
        this.initComponents(inflater, container)


        //
        this.settingClickListener()


        //
        this.initObservers()


        //
        this.loginViewModel.getUserLoginCurrent()


        // Inflate the layout for this fragment
        return binding.root
    }


    /**
     * setting components and instances
     */
    private fun initComponents(inflater: LayoutInflater, container: ViewGroup?) {
        firebaseDatabase = FirebaseDatabase.getInstance()
        firebaseFirestore = FirebaseFirestore.getInstance()

        this.binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_login, container, false
        )

        this.loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }


    private fun initObservers() {

        this.loginViewModel.userCurrentLiveData.observe(this, androidx.lifecycle.Observer {
            if (it != null && it!!.isLoged) {
                this.findNavController()?.navigate(LoginFragmentDirections.actionLoginFragmentToHome())
            }
        })

        this.loginViewModel.loginSuccess.observe(this, androidx.lifecycle.Observer { loginResult ->
            loginResult.let {
                if (loginResult!!) {
                    this.findNavController()?.navigate(LoginFragmentDirections.actionLoginFragmentToHome())
                }
            }
        })
    }


    /**
     *
     */
    private fun settingClickListener() {

        this.binding.btnRegister.setOnClickListener {
            val user = this.binding.edtxUsername.text.toString()
            val pasw = this.binding.edtxPassword.text.toString().ToHasher()

            if (!user.isEmpty() && !pasw.isEmpty()) {
                this.sendToSaveUser(user, pasw)
            }
        }


        this.binding.btnLogin.setOnClickListener {
            this.processLoginSuccess()
        }
    }


    /**
     *
     */
    private fun processLoginSuccess() {
        val username = this.binding.edtxUsername.text.toString()
        val pasw = this.binding.edtxPassword.text.toString().ToHasher()

        val collectionReference = firebaseFirestore!!.collection(AppConstants.FB_COLECTION_USERS)
        collectionReference.whereEqualTo(AppConstants.FB_COLECTION_USER_FIELD_USERNAME, username)
            .get()
            .addOnCompleteListener {

                for (message in it.result!!.iterator()) {

                    val passwordFirebase = message.getString(AppConstants.FB_COLECTION_USER_FIELD_PASSWORD)
                    if (passwordFirebase.equals(pasw)) {
                        this.loginViewModel.saveUser(UserDB(username, Date().time.toString(), true))
                        this.loginViewModel.goHome(true)
                    }
                }
            }
    }

    /**
     *
     */
    private fun sendToSaveUser(usern: String, pswd: String) {
        val user = UserFB(usern, pswd, Date())
        firebaseFirestore!!.collection(AppConstants.FB_COLECTION_USERS)
            .add(user.createUserObj())
            .addOnSuccessListener {
                Log.d("log", "User Register Success")
            }
            .addOnFailureListener {
                Log.d("log", "User Register Failure")
            }
    }


    /**
     *
     */
    fun String.ToHasher(): String {
        val bytes = this.toByteArray()
        val md = MessageDigest.getInstance("SHA-256")
        val digest = md.digest(bytes)
        return digest.fold("", { str, it -> str + "%02x".format(it) })
    }
}


