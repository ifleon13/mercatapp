package com.fleon.mercatapp.modules.makeshop.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.Utils
import com.fleon.mercatapp.databinding.SingleMakeShopItemBinding
import com.fleon.mercatapp.modules.list.view.adapter.TYPE_ACTION
import com.fleon.mercatapp.modules.makeshop.view.container.RecyclerViewContainer
import com.fleon.mercatapp.modules.makeshop.view.viewmodel.MakeShopViewModel
import kotlinx.android.synthetic.main.single_make_shop_item.view.*
import org.fabiomsr.moneytextview.MoneyTextView
import java.util.*
import kotlin.collections.HashMap
import kotlin.concurrent.schedule

enum class RowType {
    HEADER,
    ROW
}


class MakeShopAdapter(private var dataset: MutableList<RecyclerViewContainer>,
                      private val makeShopViewModel: MakeShopViewModel): RecyclerView.Adapter<MakeShopAdapter.MakeShopViewHolder>() {

    lateinit var binding: SingleMakeShopItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MakeShopViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        val inflatedView : View =
            when (viewType) {
                RowType.ROW.ordinal -> {
                    layoutInflater.inflate(R.layout.single_make_shop_item, parent,false)
                }
                else -> {
                    layoutInflater.inflate(R.layout.single_make_shop_sesion_items, parent,false)
                }
            }
        return MakeShopViewHolder(inflatedView, makeShopViewModel, dataset)
    }


    override fun getItemCount() = dataset.size



    override fun onBindViewHolder(holder: MakeShopViewHolder, position: Int) {

        val item = dataset[position]

        if (item.isHeader) {
            item.headerTitle?.let {
                holder.setHeader(R.id.sesion_text_view, item.headerTitle!!) }
        }else {

            holder.setItems(
                R.id.name_product_shop,
                R.id.cant_product_shop,
                R.id.price_product_shop,
                R.id.price_before_product_shop,
                R.id.edit_product_makeshop,
                R.id.cb_approve_shop,
                R.id.edtxt_price_shop,
                R.id.txt_signe_less_more,
                item,
                dataset)
        }




        //holder.itemView.setOnClickListener {
        //    println("Pressed at row $position")
        //}
    }


    override fun getItemViewType(position: Int): Int {
        if (dataset[position].isHeader) {
            return 0
        }else {
            return 1
        }
    }



    class MakeShopViewHolder (itemView: View, val makeShopViewModel: MakeShopViewModel, dataset: MutableList<RecyclerViewContainer>) : RecyclerView.ViewHolder(itemView) {

        private val viewMap: MutableMap<Int, View> = HashMap()

        init {
            findViewItems(itemView)
        }

        private fun findViewItems(itemView: View) {
            addToMap(itemView)
            if (itemView is ViewGroup) {
                val childCount = itemView.childCount
                (0 until childCount)
                    .map { itemView.getChildAt(it) }
                    .forEach { findViewItems(it) }
            }
        }

        private fun addToMap(itemView: View) {
            viewMap[itemView.id] = itemView
        }


        fun setHeader(@IdRes idResSession: Int, text: String) {
            val view = (viewMap[idResSession] ?: throw IllegalArgumentException("View for $idResSession not found")) as? TextView
                ?: throw IllegalArgumentException("View for $idResSession no found")

            view.text = text
        }


        fun setItems(@IdRes idResName: Int,
                     @IdRes idResCount: Int,
                     @IdRes idResValue: Int,
                     @IdRes idResValueBefore: Int,
                     @IdRes idResEdit: Int,
                     @IdRes idResCbox: Int,
                     @IdRes idResEdtxt: Int,
                     @IdRes idLessMoreEdtxt: Int,
                     item: RecyclerViewContainer,
                     dataset: MutableList<RecyclerViewContainer>) {

            //se crean las componentes definidos dentro del single_make_shop_item.xml
            val textViewName   = (viewMap[idResName]) as? TextView
            val textViewCount  = (viewMap[idResCount]) as? TextView
            val textViewValue  = (viewMap[idResValue]) as? MoneyTextView
            val textViewValueBefore  = (viewMap[idResValueBefore]) as? MoneyTextView
            val textViewEdit   = (viewMap[idResEdit]) as? TextView
            val checkbox       = (viewMap[idResCbox]) as? CheckBox
            val edtxtPrice     = (viewMap[idResEdtxt]) as? EditText
            val txtLessMore    = (viewMap[idLessMoreEdtxt]) as? TextView



            textViewName!!.text   = item.product!!.name
            textViewCount!!.text  = "${item.product!!.cant} ${item.product!!.measure}"
            val totalValue  = (item.product!!.price.toDouble() * item.product!!.cant.toDouble())
            textViewValue!!.amount = totalValue.toFloat()
            val totalPriceBefore = (item.product!!.price / item.product!!.cant)


            checkbox!!.setOnCheckedChangeListener { _, b ->
                edtxtPrice!!.isEnabled = !b
                var priceTemp = 0F
                if(edtxtPrice.text.toString().length > 0){
                    priceTemp = edtxtPrice.text.toString().toFloat()
                }
                item.product!!.priceTemp = priceTemp
                makeShopViewModel.updatePriceTempForProduct(item)
                this.calculateDiference(totalPriceBefore, priceTemp, textViewValueBefore!!, txtLessMore!!)
            }

            //editar producto
            textViewEdit!!.setOnClickListener {
                makeShopViewModel.editItem(item)
            }
        }

        private fun calculateDiference(priceBefore: Float, priceBuy: Float, textView: MoneyTextView, txtvLessMore: TextView){
            //textView!!.setTextColor(Color.RED)

            //Ahorro
         if(priceBefore > priceBuy){
             textView!!.setDecimalsColor(Color.GREEN)
             textView!!.setSymbolColor(Color.GREEN)
             textView.setBaseColor(Color.GREEN)
             textView!!.amount = priceBefore - priceBuy
             txtvLessMore!!.text = "-"
         }
         //Gasto de mas
         else if(priceBefore < priceBuy){
             textView!!.setDecimalsColor(Color.RED)
             textView!!.setSymbolColor(Color.RED)
             textView.setBaseColor(Color.RED)
             textView!!.amount = priceBuy - priceBefore
             txtvLessMore!!.text = "+"
         }
         //Igual
         else {
             textView!!.setDecimalsColor(Color.BLACK)
             textView!!.setSymbolColor(Color.BLACK)
             textView.setBaseColor(Color.BLACK)
             textView!!.amount = priceBuy
             txtvLessMore!!.text = ""
         }
        }
    }

    fun removeList(row : Int) {
        dataset.removeAt(row)
        notifyItemRemoved(row)
    }
}



