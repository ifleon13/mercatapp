package com.fleon.mercatapp.modules.list.view.viewmodel

import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.commons.database.list.ListRepositoryDB
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.SharedPreference
import com.fleon.mercatapp.commons.utils.Utils
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ListViewModel(ctx: Context, application: Application) : AndroidViewModel(application) {

    private val _moviesJob = Job()
    private val coroutineContext: CoroutineContext
        get() = _moviesJob + Dispatchers.IO


    // internal
    private val _visibility = MutableLiveData<Boolean>()
    val visibility: LiveData<Boolean>
        get() = _visibility


    private val scope = CoroutineScope(coroutineContext)
    private val repository : ListRepositoryDB = ListRepositoryDB(application)
    val listLiveData = MutableLiveData<MutableList<ListDB>>()


    private val _navigateToDetail = MutableLiveData<Int>()
    val navigateToDetail
        get() = _navigateToDetail

    val _context = ctx
    private var sharedPref: SharedPreference? = null
    var listChangeOpened = MutableLiveData<Boolean?>()


    // internal
    private val _navegationHome = MutableLiveData<Boolean>()
    //external
    val navegationHome: LiveData<Boolean>
        get() = _navegationHome


    init {
        this.sharedPref = SharedPreference(_context)
    }

    override fun onCleared() {
        super.onCleared()
        coroutineContext?.cancel()
    }


    //Funcion suspend para consultar si existe una lista actualmente abierta
    // > 0  => false;  == 0 =>  true
    suspend fun isEnableForReOpenList(): Boolean {
        Log.v(Constant.TAG_PROCESS_IS_ENABLE_REOPEN_LIST, "isEnableForReOpenList ")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        val list = repository.getListForIdWithStatus(true)
        if(list.size > 0){
            return false
        }
        return true
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processIsEnableForReOpenList(list: ListDB) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_IS_ENABLE_REOPEN_LIST)
        launch {
            val isEnable = isEnableForReOpenList()
            if(isEnable){
                confirmationEditList(list)
            }
            else{
                Utils.displayConfirmation(_context,
                    _context.getString(R.string.title_edit_list),
                    _context.getString(R.string.msn_error_reopen_list),
                    KAlertDialog.ERROR_TYPE)
            }
        }
    }



    fun fetchListWithStatus(isOpen: Boolean){
        scope.launch {
            try {
                val list = repository.getListForIdWithStatus(isOpen)
                listLiveData.postValue(list)
            } catch(e: Exception) {
                _visibility.postValue(false)
                println("Error : "+e.message.toString())
            }
        }
    }

    /**
     *
     */
    fun deleteList(idList: Int){
        scope.launch {
            repository.deleteList(idList)
        }
    }


    /**
     *
     */
    fun reOpenList(idList: Int, isOPen: Boolean){
        scope.launch {
            repository.reOpenList(idList, isOPen)
        }
    }


    /**
     *
     */
    fun setVisibleProgressBar(visible: Boolean) {
        _visibility.value = visible
    }


    /**
     *
     */
    fun onListClicked(idList: Int){
        _navigateToDetail.value = idList
    }

    /**
     * configura a null una vez que se completa la navegación
     */
    fun onListNavigated() {
        _navigateToDetail.value = null
    }


    fun goHome(isOpen: Boolean){
        listChangeOpened.postValue(isOpen)
    }


    /**
     *
     */
    fun confirmationEditList(list: ListDB) {

        val mHandler = Handler(Looper.getMainLooper())
        mHandler.post(Runnable {
            KAlertDialog(_context, KAlertDialog.WARNING_TYPE)
                .setTitleText(_context.getString(R.string.title_edit_list))
                .setContentText(_context.getString(R.string.msn_edit_list))
                .setConfirmText(_context.getString(R.string.yes_edit_list))
                .setConfirmClickListener {

                    val reOpenList = this.reOpenList(list.id, true)
                    if(reOpenList != null){
                        sharedPref!!.saveValueBoolean(Constant.KEY_LIST_ISOPENED, true)
                        it.dismissWithAnimation()
                        _navegationHome.postValue(true)
                    }
                }
                .setCancelText(_context.getString(R.string.cancel))
                .setCancelClickListener {
                    it.dismissWithAnimation()
                }
                .show()
        })
    }
}