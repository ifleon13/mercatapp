package com.fleon.mercatapp.modules.makeshop.view


import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.fleon.mercatapp.R
import com.fleon.mercatapp.databinding.FragmentMakeShopBinding
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.Utils
import com.fleon.mercatapp.modules.makeshop.adapter.MakeShopAdapter
import com.fleon.mercatapp.modules.makeshop.view.container.RecyclerViewContainer
import com.fleon.mercatapp.modules.makeshop.view.factory.MakeShopViewModelFactory
import com.fleon.mercatapp.modules.makeshop.view.viewmodel.MakeShopViewModel
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule



/**
 * A simple [Fragment] subclass.
 */
class MakeShopFragment : Fragment() {

    private lateinit var binding: FragmentMakeShopBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var makeShopViewModel: MakeShopViewModel
    private lateinit var viewModelFactory: MakeShopViewModelFactory
    private var args: MakeShopFragmentArgs? = null

    private var itemList = mutableListOf<RecyclerViewContainer>()
    private var listProductShops: MutableList<ProductListDB> = ArrayList()
    private lateinit var makeShopAdapter: MakeShopAdapter
    var mAlertDialog: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.args = MakeShopFragmentArgs.fromBundle(arguments!!)
        //confirura bindings, viewmodel, adapter y obtiene la lista
        this.initComponents(inflater, container)


        //
        this.settingFieldObservables()



        // Inflate the layout for this fragment
        return binding.root
    }



    fun initComponents(inflater: LayoutInflater, container: ViewGroup?){
        this.binding = DataBindingUtil.inflate(inflater, R.layout.fragment_make_shop, container, false)

        this.viewModelFactory = MakeShopViewModelFactory(context!!, activity!!.application)
        this.makeShopViewModel = ViewModelProviders.of(this, viewModelFactory).get(MakeShopViewModel::class.java)


        this.binding.btnCloseShop.setOnClickListener {
            endListConfirmation()
        }
    }


    /**
     * Configura los campos observables
     */
    private fun settingFieldObservables() {

        this.makeShopViewModel.editProductMakeShop.observe(this, Observer { recyclerItem ->

            var indexElementToUpdate = this.getIndexToElementToUpdate(recyclerItem)
            if(indexElementToUpdate >= 0){
                val itemUpdate = itemList.get(indexElementToUpdate)
                    updateProductConfirmation(itemUpdate.product!!)
            }
        })


        this.makeShopViewModel.updatePriceTemForProductMakeShop.observe(this, Observer { itemRecicler ->
            itemRecicler.let {
                runBlocking {
                    processSetPriceTempForProduct(itemRecicler)
                }
            }
        })
        //updateValueListShopTotalTemp
        this.makeShopViewModel.updateValueListShopTotalTemp.observe(this, Observer {
            this.binding.txtTotal.amount = it!!
        })
    }

    suspend private fun processSetPriceTempForProduct(itemRecycler: RecyclerViewContainer) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_UPDATE_PRICE_TEMP_PRODUCT_SHOP)
        launch {
            setPriceTempForProduct(itemRecycler)
            setUpdateValueListShopTotalTemp()
        }
    }

    suspend fun setPriceTempForProduct(itemRecycler: RecyclerViewContainer){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updatePriceTempForProductVM(args?.idList!!, itemRecycler)
    }

    suspend fun setUpdateValueListShopTotalTemp(){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updateTotalValueShopTemp(args?.idList!!)
    }


    fun getIndexToElementToUpdate(itemRecycler: RecyclerViewContainer): Int{
        itemList.forEachIndexed { index, element ->
            if(itemRecycler.product!!.name.equals(element.product!!.name) &&
                (itemRecycler.product!!.idCategory.toInt() == element.product!!.idCategory.toInt()) && !element.isHeader){
                return index
            }
        }
        return -1
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        runBlocking {
            processListShop(args?.idList!!)
        }
    }


    suspend private fun processListShop(idList: Int) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_LIST_PRODUCT_SHOP)
        launch {
            listProductShops = makeShopViewModel.getProductForListID(idList)
            setData()
            setRecyclerView()
        }
    }


    suspend private fun processOnlyListShop(idList: Int) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_LIST_PRODUCT_SHOP)
        launch {
            getOnlyListShop(idList)
            val list = getValueProductCalculate()
            updateValueAllProductFirebase(list)
            val listProductPriceUpdates = updateAllPriceOfListShop()
            updateAllPriceOfListShopVM(idList, listProductPriceUpdates)
            val valueTotalList = getValueTotalList(idList)
            updateValueTotalList(idList, valueTotalList)
            dismissPage()
        }
    }


    suspend fun updateValueAllProductFirebase(list: MutableList<ProductListDB>){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updateValueAllProductFirebase(list)
    }

    suspend fun updateAllPriceOfListShopVM(idList: Int, list: MutableList<ProductListDB>){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updateAllPriceOfListShopVM(idList, list)
    }

    suspend fun dismissPage(){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        activity!!.runOnUiThread(kotlinx.coroutines.Runnable {
            run {
                this.findNavController().navigateUp()
            }
        })
    }


    /**
     * Actualiza todos los precios de la lista de compras por los precio compra final pagado
     */
    suspend fun updateAllPriceOfListShop():MutableList<ProductListDB>{
        delay(Constant.TIME_DELAY_PROCESS_APP)
        for (product in listProductShops){
            if(product.priceTemp > 0){
                product.price = product.priceTemp
            }
        }
        return listProductShops
    }

    suspend fun getOnlyListShop(idList: Int){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        listProductShops = makeShopViewModel.getProductForListID(idList)
    }


    suspend fun getValueProductCalculate(): MutableList<ProductListDB>{
        val listPriceOfProductToSendToFirebase: MutableList<ProductListDB> = arrayListOf<ProductListDB>()
        var pricePerUnit = 0F
        for (product in listProductShops){

            if(product.priceTemp > 0){
                pricePerUnit = product.priceTemp / product.cant
            }
            else {
                pricePerUnit = product.price / product.cant
            }
            product.price = pricePerUnit

            Log.v(product.name, pricePerUnit.toString())
            listPriceOfProductToSendToFirebase.add(product)
        }

        return listPriceOfProductToSendToFirebase
    }


    suspend private fun setData(){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        var item: RecyclerViewContainer? = null
        itemList.clear()

        //obtiene los grupos por categorias
        val groupsCategory = listProductShops.groupBy { it.nameCategory}

        //recorre todos los grupo por categorias
        for (groupCollection in groupsCategory.values){

            //crea la sesion
            val nameHeader = groupCollection.elementAt(0).nameCategory
            item = RecyclerViewContainer(groupCollection.elementAt(0), true, nameHeader)
            itemList.add(item!!)

            //recorre cada uno de los grupos
            for(product in groupCollection.listIterator()){
                item = RecyclerViewContainer(product, false, "")

                //crea el item
                itemList.add(item!!)
            }
        }
    }


    suspend private fun setRecyclerView() {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        this.viewManager = LinearLayoutManager(this.context)
        this.makeShopAdapter = MakeShopAdapter(itemList, makeShopViewModel)

        this.recyclerView = this.binding.recyclerMakeshop.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = makeShopAdapter
            visibility = View.VISIBLE
        }
        this.binding.progressBarProducts.visibility = View.GONE
    }


    /*
     *  muestra la ventana para editar un producto seleccionado
     */
    fun updateProductConfirmation(product: ProductListDB) {

        val builder = AlertDialog.Builder(context!!)
        val inflater = LayoutInflater.from(context!!)

        val dialogLayout = inflater.inflate(R.layout.alert_dialog_setting_buy, null)
        builder.setView(dialogLayout)

        val btnAdd = dialogLayout.findViewById<Button>(R.id.btn_add)
        btnAdd.text = context!!.getString(R.string.title_btn_modified_product)

        val imgClose =  dialogLayout.findViewById<ImageView>(R.id.img_close)
        val txtvNameProduct  = dialogLayout.findViewById<TextView>(R.id.txtv_title_name_product)

        val fieldObserv  = dialogLayout.findViewById<EditText>(R.id.edtxt_observ)
        val fieldCanti   = dialogLayout.findViewById<Spinner>(R.id.spn_canti)
        val fieldMeasu   = dialogLayout.findViewById<Spinner>(R.id.spn_measurements)
        txtvNameProduct.text      = product.name

        fieldObserv.setText(product.observ)
        fieldCanti.setSelection(product.cant-1)
        fieldMeasu.setSelection(Utils.getIndexMeasures(context!!, product.measure))

        btnAdd.setOnClickListener {

            val count     = fieldCanti.getSelectedItem().toString().toInt()
            val measure = fieldMeasu.getSelectedItem().toString()
            val observ  = fieldObserv.text.toString()

            mAlertDialog!!.dismiss()

            updateProduct(product, observ, count, measure)
        }

        imgClose.setOnClickListener {
            mAlertDialog!!.dismiss()
        }

        mAlertDialog = builder.show()
    }


    /**
     * Modifica las caracteristicas del producto agregado a la compra
     */
    fun updateProduct(product: ProductListDB, obsv: String, cant: Int, measure: String){

        //si algunos de los datos del producto es modificado
        if(!product.observ.equals(obsv)
            || product.cant != cant
            || !product.measure.equals(measure)){

            runBlocking {
                processUpdateDataProduct(cant, measure, obsv, product.idList, product.id)
            }
        }
    }


    /**
     * Funcion que actualiza los datos del producto
     */
    suspend fun updateDataProduct(cant: Int, measure: String, observ: String, idList: Int, idProduct: Int) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updateDataProduct(cant, measure, observ, idList, idProduct)
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processUpdateDataProduct(cant: Int, measure: String, observ: String, idList: Int, idProduct: Int) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_UPDATE_PRODUCT_OF_LIST_SHOP)
        launch {
            updateDataProduct(cant, measure, observ, idList, idProduct)
            setData()
            updateCountProductForList(idList)
            val valueTotalList = getValueTotalList(idList)
            updateValueTotalList(idList, valueTotalList)
            reloadFragment()
        }
    }


    suspend fun updateCountProductForList(idList: Int){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updateCountProductForListVM(idList)
    }

    suspend fun getValueTotalList(idList: Int): Float{
        delay(Constant.TIME_DELAY_PROCESS_APP)
        return makeShopViewModel.getValueTotalListVM(idList)
    }


    suspend fun updateValueTotalList(idList: Int, valueList: Float){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        makeShopViewModel.updateValueTotalListVM(idList, valueList)
    }

    suspend fun reloadFragment(){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        activity!!.runOnUiThread(Runnable {
            run {
                this.findNavController().navigateUp()
            }
        })
    }



    /**
     * Muestra un cuadro de informacion al usuario para cerrar la lista
     */
    fun endListConfirmation() {
        KAlertDialog(context!!, KAlertDialog.WARNING_TYPE)
            .setTitleText("Finalizar Compra")
            .setContentText("Desea Finalizar la compra ahora?")
            .setConfirmText(context!!.getString(R.string.title_continue))
            .setConfirmClickListener {

                runBlocking {
                    processOnlyListShop(args?.idList!!)
                }
                it.dismissWithAnimation()
            }
            .setCancelText(context!!.getString(R.string.cancel))
            .setCancelClickListener {
                it.dismissWithAnimation()
            }
            .show()
    }

}
