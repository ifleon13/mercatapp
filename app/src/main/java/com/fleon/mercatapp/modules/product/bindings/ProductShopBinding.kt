package com.fleon.mercatapp.modules.product.bindings

import android.util.Log
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import org.fabiomsr.moneytextview.MoneyTextView
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.roundToLong

@BindingAdapter("productShopedName")
fun TextView.setProductShopedName(item: ProductListDB?) {
    item?.let {
        text = item?.name
    }
}


@BindingAdapter("productShopedCant")
fun TextView.setProductShopedCant(item: ProductListDB?) {
    item?.let {
        text = item?.cant.toString()
    }
}


@BindingAdapter("productShopedValue")
fun MoneyTextView.setProductShopedValue(item: ProductListDB?) {
    item?.let {
        this.amount = item?.price
    }
}



