package com.fleon.mercatapp.modules.list.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.commons.database.list.ListRepositoryDB
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.SharedPreference
import com.fleon.mercatapp.commons.utils.Utils
import com.fleon.mercatapp.databinding.FragmentListBinding
import com.fleon.mercatapp.databinding.SingleListHistoryItemBinding
import com.fleon.mercatapp.modules.list.view.adapter.ListAdapter
import com.fleon.mercatapp.modules.list.view.adapter.ListListener
import com.fleon.mercatapp.modules.list.view.adapter.TYPE_ACTION
import com.fleon.mercatapp.modules.list.view.factory.ListViewModelFactory
import com.fleon.mercatapp.modules.list.view.viewmodel.ListViewModel
import com.fleon.mercatapp.modules.login.views.LoginFragmentDirections
import com.fleon.mercatapp.modules.product.views.ProductFragmentDirections
import com.google.android.material.snackbar.Snackbar
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking


/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {

    private lateinit var listViewModel: ListViewModel
    private lateinit var binding: FragmentListBinding
    private lateinit var viewModelFactory: ListViewModelFactory
    private var listAdapter: ListAdapter? = null
    private var allList: MutableList<ListDB> = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //confirura bindings, viewmodel, adapter y obtiene la lista
        this.initComponents(inflater, container)


        //asigna los campos observables
        this.settingFieldObservables()

        // Inflate the layout for this fragment
        return binding.root
    }


    fun initComponents(inflater: LayoutInflater, container: ViewGroup?){
        this.binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false)

        this.viewModelFactory = ListViewModelFactory(context!!, activity!!.application)
        this.listViewModel    = ViewModelProviders.of(this, viewModelFactory).get(ListViewModel::class.java)

        this.binding.lifecycleOwner = this

        //asigna el viewmodel creado, al viewmodel definido en el data del xml
        this.binding.vmList = this.listViewModel


        //configura el adapter pasandole el listener y el tipo de accion para eventos sobre cada Item
        this.listAdapter = ListAdapter(ListListener { list, type ->

            when (type) {
                TYPE_ACTION.DELETE -> {
                    var index = this.getIndexListSelected(list)

                    if(index >= 0) {
                        this.confirmationDeleteList(index, list.id)
                    }else {
                        this.displayMsnError()
                    }
                }
                TYPE_ACTION.EDIT -> {
                    runBlocking {
                        listViewModel.processIsEnableForReOpenList(list)
                    }
                }
                else -> {
                   this.listViewModel.onListClicked(list.id)
                }
            }
        })


        setUpRecyclerView(container!!.rootView)
        this.listViewModel.setVisibleProgressBar(true)
        this.listViewModel.fetchListWithStatus(false)
    }


    /**
     * Configura el reciclerView
     */
    fun setUpRecyclerView(view: View){
        this.binding.recyclerList.setHasFixedSize(true)
        this.binding.recyclerList.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        this.listAdapter!!.listAdapter(this.allList)
    }


    /**
     * Configura los campos observables
     */
    fun settingFieldObservables(){

        this.listViewModel.listLiveData.observe(this, Observer {
            //TODO - Your Update UI Logic
            this.allList.clear()
            this.allList.addAll(it)

            this.listAdapter?.updateList(it)
            this.binding.recyclerList.adapter = this.listAdapter

            this.listViewModel.setVisibleProgressBar(false)
        })


        this.listViewModel.navigateToDetail.observe(this, Observer { idList ->
            idList?.let {
                this.findNavController()?.navigate(ListFragmentDirections.actionListFragment2ToMakeShopFragment(idList))
                this.listViewModel.onListNavigated()
            }
        })


        this.listViewModel.visibility.observe(this, Observer {visibilibity: Boolean ->
            if(visibilibity){
                this.binding.progressBarList.visibility = View.VISIBLE
            }
            else {
                this.binding.progressBarList.visibility = View.GONE
            }
        })


        //navega hacia el home de la App
        this.listViewModel.navegationHome.observe(this, Observer {
            val snack = Snackbar.make(this.view!!,
                context!!.getString(R.string.msn_success_reopen_list), Snackbar.LENGTH_LONG)
            snack.show()
            listViewModel.goHome(true)
        })


        this.listViewModel.listChangeOpened.observe(this, Observer {listOpened ->
            listOpened.let {
                    activity!!.runOnUiThread(kotlinx.coroutines.Runnable {
                        run {
                            this.findNavController().navigateUp()
                        }
                    })
            }
        })
    }


    /**
     *
     */
    fun getIndexListSelected(listCurrent: ListDB):Int{
        allList.forEachIndexed { index, element ->
            if(listCurrent.id == element.id){return index}
        }
        return -1
    }


    /**
     *
     */
    fun confirmationDeleteList(index: Int, idList: Int) {
        KAlertDialog(context!!, KAlertDialog.WARNING_TYPE)
            .setTitleText(context!!.getString(R.string.delete_list))
            .setContentText(context!!.getString(R.string.msn_quiestion_delete_list))
            .setConfirmText(context!!.getString(R.string.yes_delete))
            .setConfirmClickListener {

                val delete = this.listViewModel.deleteList(idList)
                if(delete != null){
                    this.listAdapter?.removeList(index)
                    it.dismissWithAnimation()
                }
            }
            .setCancelText(context!!.getString(R.string.cancel))
            .setCancelClickListener {
                it.dismissWithAnimation()
            }
            .show()
    }



    /**
     *
     */
    fun displayMsnError(){
        val snack = Snackbar.make(this.view!!,
            context!!.getString(R.string.error_delete_list), Snackbar.LENGTH_LONG)
        snack.show()
    }
}
