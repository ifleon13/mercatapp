package com.fleon.mercatapp.modules.login.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.commons.database.user.UserDB
import com.fleon.mercatapp.commons.database.user.UserDBRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class LoginViewModel(application: Application) : AndroidViewModel(application) {


    private val _moviesJob = Job()
    private val coroutineContext: CoroutineContext
        get() = _moviesJob + Dispatchers.IO

    private val scope = CoroutineScope(coroutineContext)
    private val repository  = UserDBRepository(application)
    var userCurrentLiveData = MutableLiveData<UserDB?>()
    var loginSuccess = MutableLiveData<Boolean?>()


    fun saveUser(user: UserDB) {
        scope.launch {
            try {
                repository.insertUser(user)
            } catch (e: Exception) {
                println("Error : " + e.message.toString())
            }
        }
    }


    fun getUserLoginCurrent() {
        scope.launch {
            try {
                val user = repository.getLoginUserWithNickName()
                userCurrentLiveData.postValue(user)
            } catch(e: Exception) {
                println("Error : "+e.message.toString())
            }
        }
    }


    fun goHome(loginStatus: Boolean){
        loginSuccess.postValue(loginStatus)
    }
}


