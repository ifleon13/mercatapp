package com.fleon.mercatapp.modules.product.factory

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fleon.mercatapp.modules.product.viewmodels.ProductViewModel

class ProductViewModelFactory(private val idCategory: String, private val ctx: Context, val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProductViewModel::class.java)) {
            return ProductViewModel(idCategory, ctx, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}