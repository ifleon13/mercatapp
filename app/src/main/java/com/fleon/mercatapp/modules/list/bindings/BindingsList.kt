package com.fleon.mercatapp.modules.list.view.bindings

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.fleon.mercatapp.commons.database.list.ListDB
import org.fabiomsr.moneytextview.MoneyTextView
import java.math.RoundingMode
import java.text.DecimalFormat


@BindingAdapter("bindingListName")
fun TextView.bindingListName(item: ListDB?) {
    item?.let {
        text = item?.namelist.capitalize()
    }
}


@BindingAdapter("bindingListDate")
fun TextView.bindingListDate(item: ListDB?) {
    item?.let {
        text = item?.date
    }
}


@BindingAdapter("bindingListValue")
fun MoneyTextView.bindingListValue(item: ListDB?) {
    item?.let {
        this.amount = item?.value

    }
}


@BindingAdapter("bindingListCountProducts")
fun TextView.bindingListCountProducts(item: ListDB?) {
    item?.let {
        text = "(${item?.countProducts} productos)"
    }
}