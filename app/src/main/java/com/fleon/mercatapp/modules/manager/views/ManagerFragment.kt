package com.fleon.mercatapp.modules.manager.views


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.fleon.mercatapp.R
import com.fleon.mercatapp.databinding.FragmentManagerBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ManagerFragment : Fragment() {

    private lateinit var binding: FragmentManagerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //
        this.initComponents(inflater, container)


        //
        this.settingClickListener()


        //
        return this.binding.root
    }


    /**
     *
     */
    private fun initComponents(inflater: LayoutInflater, container: ViewGroup?){

        //hiden back button toolbar
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(false)


        this.binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_manager, container, false)
    }



    /**
     *
     */
    private fun settingClickListener(){

        this.binding.btnCreateNewList.setOnClickListener {
            this.findNavController()?.navigate(ManagerFragmentDirections.actionManagerFragmentToCreateFragment())
        }


        this.binding.btnReviewList.setOnClickListener {
            this.findNavController()?.navigate(ManagerFragmentDirections.actionManagerFragmentToListFragment2())
        }
    }

}
