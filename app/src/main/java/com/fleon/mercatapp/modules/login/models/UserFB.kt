package com.fleon.mercatapp.modules.login.models

import com.fleon.mercatapp.commons.utils.AppConstants
import java.util.*
import kotlin.collections.HashMap

data class UserFB(var usern: String, var pswd: String, var date: Date){

    fun createUserObj(): Map<String, Any> {
        val userObj = HashMap<String, Any>()
        userObj[AppConstants.FB_COLECTION_USER_FIELD_USERNAME] = this.usern
        userObj[AppConstants.FB_COLECTION_USER_FIELD_PASSWORD] = this.pswd
        userObj[AppConstants.FB_COLECTION_USER_FIELD_DATE] = this.date
        return userObj
    }

}