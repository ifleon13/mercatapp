package com.fleon.mercatapp.modules.product.views


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.databinding.FragmentProductBinding
import com.fleon.mercatapp.modules.product.adapters.ListProductShopAdapter
import com.fleon.mercatapp.modules.product.adapters.ProductAdapter
import com.fleon.mercatapp.modules.product.adapters.ProductListener
import com.fleon.mercatapp.modules.product.adapters.ProductShopListener
import com.fleon.mercatapp.modules.product.factory.ProductShopViewModelFactory
import com.fleon.mercatapp.modules.product.factory.ProductViewModelFactory
import com.fleon.mercatapp.commons.database.product.ProductDB
import com.fleon.mercatapp.modules.product.viewmodels.ListProductShopViewModel
import com.fleon.mercatapp.modules.product.viewmodels.ProductViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.runBlocking
import java.math.RoundingMode
import java.text.DecimalFormat


class ProductFragment : Fragment() {

    private lateinit var binding: FragmentProductBinding
    private lateinit var productViewModel: ProductViewModel
    private var firebaseFirestore: FirebaseFirestore? = null
    private var listProducts: MutableList<ProductDB> = ArrayList()
    private var pAdapter: ProductAdapter? = null
    private lateinit var viewModelFactory: ProductViewModelFactory
    private var args: ProductFragmentArgs? = null

    //list shop products
    private lateinit var listProductShopViewModel: ListProductShopViewModel
    private lateinit var viewModelShopFactory: ProductShopViewModelFactory
    private var listProductShops: MutableList<ProductListDB> = ArrayList()
    private var pShopdAdapter: ListProductShopAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.args = ProductFragmentArgs.fromBundle(arguments!!)

        //
        this.initComponents(inflater, container)


        //
        this.settingFieldObservables()


        //
        this.getNameCategory()


        //
        this.getListProductsForCategory()


        //
        this.getListOpenProductSelectForShop()


        //
        return this.binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
               super.onCreateOptionsMenu(menu, inflater)
               inflater?.inflate(R.menu.menu_product, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.getItemId()

        if (id == R.id.delete_list) {
            this.productViewModel.deleteListCurrent()
            return true
        }
        if (id == R.id.action_add_product) {
            this.productViewModel.getDataForCreateProductOnFirebase()
            return true
        }

        return super.onOptionsItemSelected(item)

    }


    private fun initComponents(inflater: LayoutInflater, container: ViewGroup?){

        this.firebaseFirestore = FirebaseFirestore.getInstance()

        this.binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_product, container, false)

        this.binding.lifecycleOwner = this

        this.viewModelFactory = ProductViewModelFactory(args?.idCategory!!, context!!, activity!!.application)
        this.productViewModel    = ViewModelProviders.of(this, viewModelFactory).get(ProductViewModel::class.java)

        this.viewModelShopFactory = ProductShopViewModelFactory(context!!, activity!!.application)
        this.listProductShopViewModel  = ViewModelProviders.of(this, viewModelShopFactory).get(ListProductShopViewModel::class.java)

        //configura el adapter pasandole el listener para eventos sobre cada Item
        this.pAdapter = ProductAdapter(ProductListener { product ->
            this.productViewModel.addProductConfirmation(product)
        })


        //muestra las opciones para editar el producto seleccionado
        this.pShopdAdapter = ListProductShopAdapter(ProductShopListener { productShop, isEditing ->
            productShop.let {
                if(isEditing){
                    this.listProductShopViewModel.updateProductConfirmation(it)
                }
                else{
                    this.listProductShopViewModel.deleteProductShopFromList(it)
                }
            }
        })


        // Envia los productos de la lista a la lista de firebase
        this.binding.imgAproveList.setOnClickListener{
            this.confirmationApproveList()
        }
        //habilita el boton del back de la toolbar
        setHasOptionsMenu(true)
    }


    /**
     *
     */
    fun confirmationApproveList() {
        KAlertDialog(context!!, KAlertDialog.WARNING_TYPE)
            .setTitleText(context!!.getString(R.string.title_approve_list))
            .setContentText(context!!.getString(R.string.msn_approve_list))
            .setConfirmText(context!!.getString(R.string.yes_end_list))
            .setConfirmClickListener {
                this.productViewModel.sendListDetailProductForUser()
                it.dismissWithAnimation()
            }
            .setCancelText(context!!.getString(R.string.cancel))
            .setCancelClickListener {
                it.dismissWithAnimation()
            }
            .show()
    }


    /**
     * Ontiene el nombre de la categoria seleccionada
     */
    private fun getNameCategory(){
        runBlocking {
            productViewModel.processNameCategory()
        }
    }


    /**
     * Obtiene la lista de productos, correspondiente a la categoria seleccionada
     */
    private fun getListProductsForCategory(){
        runBlocking {
            productViewModel.getListProductFirebase(firebaseFirestore)
        }
    }


    /**
     * Obtiene el listado de productos que han sido agregados a la unica lista con estado abierta
     */
    private fun getListOpenProductSelectForShop(){
        this.listProductShopViewModel.getListOpenProductForShop()
    }


    /**
     * Configura los campos observables
     */
    private fun settingFieldObservables() {

        //Controla la visibilidad del dialogo de progresp
        this.productViewModel.visibilityProgressBar.observe(this, Observer { visibilibity: Boolean ->
                if (visibilibity) {
                    this.binding.progressBarProducts.visibility = View.VISIBLE
                } else {
                    this.binding.progressBarProducts.visibility = View.GONE
                }
            })


        //actualiza el adapter de productos
        this.productViewModel.productListLiveData.observe(this, Observer {
            this.listProducts.clear()
            this.listProducts.addAll(it)

            this.pAdapter?.updatePostingCategorys(it)
            this.binding.recyclerCategory.adapter = this.pAdapter

            this.productViewModel.setVisibleProgressBar(false)
        })


        //Actualiza el titulo de la barra, con el nombre de la nueva categoria
        this.productViewModel.getNameCategoryObserver().observe(this, Observer { name ->
            name.let {
                (activity as AppCompatActivity).supportActionBar?.title = it
            }
        })


        //Informa que ya existe una lista abierta
        this.productViewModel.isToastErrorListExist().observe(this, Observer { isListExist ->
            isListExist.let {

                if(it){
                    val snack = Snackbar.make(this.view!!,
                        getString(R.string.error_recreate_list), Snackbar.LENGTH_LONG)
                    snack.show()
                }
            }
        })


        //actualiza el adapter de compra de productos con la lista de producto anadidos a una compra
        this.listProductShopViewModel.productListLiveData.observe(this, Observer {
            //TODO - Your Update UI Logic
            this.listProductShops.clear()
            this.listProductShops.addAll(it)

            this.pShopdAdapter?.updatePostingProductShop(it)
            this.binding.recyclerCarShop.adapter = this.pShopdAdapter


            var valueTotalSop = 0F;
            for(item in it){
                valueTotalSop += item.price * item.cant
            }

            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            df.format(valueTotalSop)

            //Actualiza el valor total de la compra
            this.binding.txtValueShop.amount = valueTotalSop
        })


        //Actualiza la lista de productos anexados a una lista de compra desde listProductShopViewModel
        this.listProductShopViewModel.updateListProductShop.observe(this, Observer {
            if(it){
                this.listProductShopViewModel.getListOpenProductForShop()
            }

        })


        //Actualiza la lista de productos anexados a una lista de compra desde productViewModel
        this.productViewModel.updateListProductShop.observe(this, Observer {
            if(it){
                this.listProductShopViewModel.getListOpenProductForShop()
            }
        })


        //actualiza la lista de productos desde Firebase
        this.productViewModel.updateListProductFirebase.observe(this, Observer {
            runBlocking {
                productViewModel.getListProductFirebase(firebaseFirestore)
            }
        })


        //actualiza la lista de productos desde Firebase
        this.productViewModel.navegationHome.observe(this, Observer {
            this.findNavController()?.navigate(ProductFragmentDirections.actionProductFragmentToManagerFragment2())
        })
    }
}
