package com.fleon.mercatapp.modules.makeshop.view.factory

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fleon.mercatapp.modules.makeshop.view.viewmodel.MakeShopViewModel


class MakeShopViewModelFactory(private val ctx: Context, val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MakeShopViewModel::class.java)) {
            return MakeShopViewModel(ctx, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}