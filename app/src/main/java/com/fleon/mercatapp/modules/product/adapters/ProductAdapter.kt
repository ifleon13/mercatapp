package com.fleon.mercatapp.modules.product.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fleon.mercatapp.databinding.SingleProductItemBinding
import com.fleon.mercatapp.commons.database.product.ProductDB

class ProductAdapter(val clickListener: ProductListener): RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    var allListProducts: MutableList<ProductDB>  = ArrayList()


    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val item = allListProducts.get(position)
        holder.bind(item, clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder.from(parent)
    }


    override fun getItemCount(): Int {
        return allListProducts.size
    }


    class ProductViewHolder private constructor(val binding: SingleProductItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(producto: ProductDB, clickListener: ProductListener){
            binding.pt = producto
            binding.executePendingBindings()
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ProductViewHolder {
                val lyInflater = LayoutInflater.from(parent.context)
                val binding = SingleProductItemBinding.inflate(lyInflater, parent, false)
                return ProductViewHolder(binding)
            }
        }
    }


    fun updatePostingCategorys(newpostingCategory: List<ProductDB>) {

        val diffCallback = ProductDiffCallback(allListProducts, newpostingCategory)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        allListProducts.clear()
        allListProducts.addAll(newpostingCategory)
        diffResult.dispatchUpdatesTo(this)

    }


    fun removeCategories(row : Int) {
        allListProducts.removeAt(row)
        notifyItemRemoved(row)
    }


    fun addCategories(row : Int, product: ProductDB) {
        allListProducts.add(product)
        notifyItemInserted(row)
    }


    /**
     * Esta clase verifica los elementos para ver si son del mismo tipo o tienen los mismos valores
     */
    class ProductDiffCallback(private val oldList: List<ProductDB>,
                              private val newList: List<ProductDB>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        //verifica el tipo de fila
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] === newList.get(newItemPosition)
        }

        //verifica para ver si las filas son iguales
        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return oldList[oldPosition] == newList[newPosition]
        }
    }
}


/**
 * Listener de eventos para el cardView
 */
class ProductListener(val clickListener: (product: ProductDB) -> Unit) {
    fun onClickProduct(product: ProductDB) = clickListener(product)
}