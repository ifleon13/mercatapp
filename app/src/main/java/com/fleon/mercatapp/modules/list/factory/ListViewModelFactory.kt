package com.fleon.mercatapp.modules.list.view.factory

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fleon.mercatapp.modules.list.view.viewmodel.ListViewModel

class ListViewModelFactory(private val ctx: Context, val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListViewModel::class.java)) {
            return ListViewModel(ctx, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}