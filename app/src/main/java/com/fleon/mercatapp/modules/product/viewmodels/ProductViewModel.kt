package com.fleon.mercatapp.modules.product.viewmodels

import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.category.CategoryDBRepository
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.commons.database.list.ListRepositoryDB
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.database.products_list.ProductListRepositoryDB
import com.fleon.mercatapp.commons.database.user.UserDB
import com.fleon.mercatapp.commons.database.user.UserDBRepository
import com.fleon.mercatapp.commons.utils.AppConstants
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.Constant.Companion.MSN_DELETE_TAG_SUCCESS_IN_FIREBASE
import com.fleon.mercatapp.commons.utils.SharedPreference
import com.fleon.mercatapp.commons.utils.Utils
import com.fleon.mercatapp.commons.database.product.ProductDB
import com.fleon.mercatapp.commons.database.product.ProductRepositoryDB
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.*
import java.lang.Runnable
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext


class ProductViewModel(idCategory: String, ctx: Context, application: Application) : AndroidViewModel(application) {

    private val _moviesJob = Job()
    private val coroutineContext: CoroutineContext
        get() = _moviesJob + Dispatchers.IO

    private val scope = CoroutineScope(coroutineContext)

    // internal
    private val _visibilityProgressBar = MutableLiveData<Boolean>()
    //external
    val visibilityProgressBar: LiveData<Boolean>
        get() = _visibilityProgressBar

    val productListLiveData =  MutableLiveData<MutableList<ProductDB>>()
    private var _openListLiveData: MutableList<ListDB>? = null
    private val sharedPref: SharedPreference = SharedPreference(ctx)

    //internal
    val _idCategory = idCategory
    val _context = ctx

    private val repositoryList         = ListRepositoryDB(application)
    private val repositoryCat          = CategoryDBRepository(application)
    private val repositoryProdToList   = ProductListRepositoryDB(application)
    private val repositoryUser         = UserDBRepository(application)
    private val repositoryProdListShop = ProductListRepositoryDB(application)
    private val productRepositoryDB = ProductRepositoryDB(application)
    private var idListCurrent: Int     = 0;
    private var fullNameCategory       = MutableLiveData<String>()
    private var listOpened: Boolean    = false
    private var _toastErrorListExist    = MutableLiveData<Boolean>()

    // internal
    private val _updateListProductShop = MutableLiveData<Boolean>()
    //external
    val updateListProductShop: LiveData<Boolean>
        get() = _updateListProductShop

    // internal
    private val _updateListProductFirebase = MutableLiveData<Boolean>()
    //external
    val updateListProductFirebase: LiveData<Boolean>
        get() = _updateListProductFirebase

    // internal
    private val _navegationHome = MutableLiveData<Boolean>()
    //external
    val navegationHome: LiveData<Boolean>
        get() = _navegationHome


    private var lastNumberIDProducto: Int = 0

    // internal
    private var _updateValueTotalShop = MutableLiveData<Boolean>()
    //external
    val updateValueTotalShop: LiveData<Boolean>
        get() = _updateValueTotalShop

    val firebaseFirestore     = FirebaseFirestore.getInstance()

    init {
        this.idListCurrent = sharedPref.getValueInt(Constant.KEYID_LIST_CURRENT)
        this.listOpened    = sharedPref.getValueBoolean(Constant.KEY_LIST_ISOPENED, false)
        Log.i("_idCategory", "Final Scor_idCategory is $_idCategory")
    }



    /**
     * Observer Metodo para informar la carga de lista de categorias por ID
     */
    suspend fun getNameCategory() {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        fullNameCategory.postValue(repositoryCat.getNameCategoryById(_idCategory))
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processNameCategory() = withContext(Dispatchers.IO) {
        launch { getNameCategory() }
    }



    /**
     *  Observer Metodo para informar el nombre de la categoria
     */
    fun getNameCategoryObserver(): LiveData<String> {
        return fullNameCategory
    }



    /**
     *  Observer Metodo para informar que ya existe una lista abierta
     */
    fun isToastErrorListExist(): LiveData<Boolean> {
        return _toastErrorListExist
    }


    /*
     *  muestra la confirmacion para agregar un nuevo producto a la lista
     */
    fun addProductConfirmation(product: ProductDB) {
        this.listOpened    = sharedPref.getValueBoolean(Constant.KEY_LIST_ISOPENED, false)

        if(this.listOpened){
            var  mAlertDialog: AlertDialog? = null
            val builder = AlertDialog.Builder(_context)
            val inflater = LayoutInflater.from(_context)

            val dialogLayout =
                inflater.inflate(R.layout.alert_dialog_setting_buy, null)
            builder.setView(dialogLayout)

            val btnAdd =
                dialogLayout.findViewById<Button>(R.id.btn_add)

            val imgClose =  dialogLayout.findViewById<ImageView>(R.id.img_close)
            val txtvNameProduct  = dialogLayout.findViewById<TextView>(R.id.txtv_title_name_product)
            val txtvNameDescrip  = dialogLayout.findViewById<TextView>(R.id.txtv_descrip_product)
            txtvNameProduct.text = product.name
            txtvNameDescrip.text = product.description

            val fieldObserv  = dialogLayout.findViewById<EditText>(R.id.edtxt_observ)
            val fieldCanti   = dialogLayout.findViewById<Spinner>(R.id.spn_canti)
            val fieldMeasu   = dialogLayout.findViewById<Spinner>(R.id.spn_measurements)
            val fieldPrice   = dialogLayout.findViewById<EditText>(R.id.edtxt_price_setting)

            // por defecto opcion de Unidad
            fieldMeasu.setSelection(2)

            btnAdd.setOnClickListener {
                val canti    = fieldCanti.getSelectedItem().toString().toInt();
                val measure  = fieldMeasu.getSelectedItem().toString()
                val observ   = fieldObserv.text.toString()
                val price    = if (fieldPrice.text.toString().isNotEmpty()) fieldPrice.text.toString().toFloat() else 0F

                mAlertDialog!!.dismiss()
                addProductToList(product, canti, measure, observ, price)
            }


            imgClose.setOnClickListener {
                mAlertDialog!!.dismiss()
            }

            mAlertDialog = builder.show()
        }
        else {
            addListConfirmation()
        }
    }


     // Agrega un nuevo producto a la lista actual
    fun addProductToList(product: ProductDB, cant: Int, measure: String, observ: String, price: Float){
        this.idListCurrent = sharedPref.getValueInt(Constant.KEYID_LIST_CURRENT)

        val productListDB = ProductListDB(this.idListCurrent,
            product.id.toInt(),
            cant, measure, observ, product.name, price, _idCategory, fullNameCategory.value.toString(),
            true, "")

            runBlocking {
                processInsertProductToList(productListDB)
            }
    }



    // Funcion suspend para insertar un nuevo producto
    suspend fun insertProductToList(productListDB: ProductListDB) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryProdToList.insertProductToList(productListDB)
    }


    // Funcion suspend para actualizar al valor total de la compra
    suspend fun updateListProductShop(value: Boolean) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        _updateListProductShop.postValue(value)
    }



    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processInsertProductToList(productListDB: ProductListDB) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_INSERT_PRODUCT_TO_LIST_SHOP)
        launch {
            insertProductToList(productListDB)
            updateListProductShop(true)
            updateValueProductFirebase(productListDB)
            updateListaFromFirebase()
        }
    }


    suspend fun updateListaFromFirebase(){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        _updateListProductFirebase.postValue(true)
    }

     //Funcion suspend que obtine la lista de productos desde firebase filtrado por _idCategory
    suspend fun getListProductFirebase(firebaseFirestore: FirebaseFirestore?) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        val collectionReference = firebaseFirestore!!.collection(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES)
        val itemList: MutableList<ProductDB> = ArrayList()

        collectionReference
            .orderBy(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_NAME)
            .whereEqualTo(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_ID_CATEGORY, _idCategory)
            .get()
            .addOnCompleteListener {
                for (product in it.result!!.iterator()){
                    Log.d("ID_FIREBASE: ", product.id.toString())
                    Log.d("log", product.getString(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_NAME))

                    val newProduct = ProductDB()
                    newProduct.id            = product.getString(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_ID)!!
                    newProduct.idCategory    = product.getString(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_ID_CATEGORY)!!
                    newProduct.name          =  product.getString(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_NAME)!!
                    newProduct.image         = product.getString(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_IMAGE)!!
                    newProduct.price         = product.getDouble(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_PRICE)!!.toFloat()!!
                    newProduct.idChildFirebase = product.id
                    newProduct.description     = product.getString(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES_FIELD_DESCRIPTION)!!

                    itemList.add(newProduct)
                }

                runBlocking {
                    Log.v(Constant.TAG_APP, "processProductOfFirebase")
                    setPostValueProducts(itemList)
                }
            }
    }




    //Funcion suspend que obtiene el ultimo ID de los productos de Firebase para esta categoria,
    suspend fun saveList(itemList: MutableList<ProductDB>) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        for(product in itemList){
            productRepositoryDB.insertProduct(product)
        }
    }


    //Funcion suspend que obtiene el ultimo ID de los productos de Firebase para esta categoria,
    suspend fun getIDProductInFirebase(itemList: MutableList<ProductDB>) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        lastNumberIDProducto = getLastIDProductFirebase(itemList)
    }


    //Funcion suspend que actualiza el item seleccionado de la lista
    suspend fun setPostValueProducts(itemList: MutableList<ProductDB>) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        productListLiveData.postValue(itemList)
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processProductOfFirebase(itemList: MutableList<ProductDB>) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_INSERT_PRODUCT_TO_LIST_SHOP)
        launch {
            getIDProductInFirebase(itemList)
            setPostValueProducts(itemList)
            saveList(itemList)
        }
    }



    /**
     * Obtiene el ultimo ID asignado a un producto
     */
    fun getLastIDProductFirebase(itemList: MutableList<ProductDB>): Int{
        var lastID: Int = 0

        //si no se tiene productos para esta categoria, crea un id base a partir del Id de la categoria.
        if(itemList.size == 0){
            val newBaseId: String = "$_idCategory"+"0"
            lastID = newBaseId.toInt()
        }
        else{
            for (item in itemList){
                if(item.id.toInt() > lastID){
                    lastID = item.id.toInt()
                }
            }
        }
        return lastID
    }



    /*
     *   Muestra u oculta el dialogo de progreso
     */
    fun setVisibleProgressBar(visible: Boolean) {
        _visibilityProgressBar.value = visible
    }



    /*
     * Muestra el cuadro de confirmacion para guardar una nueva lista
     */
    fun addListConfirmation() {
        this.listOpened    = sharedPref.getValueBoolean(Constant.KEY_LIST_ISOPENED, false)

        if(!this.listOpened){
            val mHandler = Handler(Looper.getMainLooper())
            mHandler.post(Runnable {
                // Your UI updates here
                var  mAlertDialog: AlertDialog? = null
                val builder = AlertDialog.Builder(_context)
                val inflater = LayoutInflater.from(_context)

                val dialogLayout =
                    inflater.inflate(R.layout.alert_dialog_add_list, null)
                builder.setView(dialogLayout)

                val btnAdd    = dialogLayout.findViewById<Button>(R.id.btn_add_list)
                val btnCancel =  dialogLayout.findViewById<Button>(R.id.btn_cancel_add_list)
                val edxtNameList = dialogLayout.findViewById<EditText>(R.id.edtxt_add_list)
                val chkbox    = dialogLayout.findViewById<CheckBox>(R.id.cb_asign_date)

                chkbox.setOnClickListener {
                    if(chkbox.isChecked){
                        edxtNameList.setText(Utils.getDataFornameList())
                    }
                    else{
                        edxtNameList.setText("")
                    }
                }

                btnAdd.setOnClickListener {
                    if(!edxtNameList.text.toString().isEmpty()){
                        this.saveNewList(edxtNameList.text.toString())

                        mAlertDialog!!.dismiss()
                    }else{
                        Utils.displayConfirmation(_context, _context.getString(R.string.create_new_list),
                            _context.getString(R.string.title_error_name_list_empty),
                            KAlertDialog.ERROR_TYPE)
                    }
                }

                btnCancel.setOnClickListener {
                    mAlertDialog!!.dismiss()
                }

                mAlertDialog = builder.show()
            })
        }
        else {
            _toastErrorListExist.postValue(true)
        }
    }



    /*
     *  Elimina la lista actual de ROOM y de Firebase si detecta conexion a internet
     */
    fun deleteListCurrent(){
        this.idListCurrent = sharedPref.getValueInt(Constant.KEYID_LIST_CURRENT)

        if(this.idListCurrent > 0){
            scope.launch {
                val deleteList = repositoryProdToList.deleteProductToList(idListCurrent)

                if(deleteList != null){

                    //Si hay conexion a internet, intenta eliminarla de firebase
                    if(Utils.isInternetOn(getApplication())){
                        deleteListInFirebase()
                    }
                    else{
                        Log.v(Constant.TAG_PENDING_IN_FIREBASE, Constant.MSN_DELETE_TAG_PENDING_IN_FIREBASE)
                    }

                    sharedPref.saveValueBoolean(Constant.KEY_LIST_ISOPENED, false)
                    Utils.displayConfirmation(_context, _context.getString(R.string.delete_list),
                        _context.getString(R.string.msn_list_deleted_success),
                        KAlertDialog.SUCCESS_TYPE)
                }
                else {
                    Utils.displayConfirmation(_context, _context.getString(R.string.delete_list),
                        _context.getString(R.string.title_error_delete_list),
                        KAlertDialog.ERROR_TYPE)
                }
            }
        }
        else {
            Utils.displayConfirmation(_context, _context.getString(R.string.delete_list),
                _context.getString(R.string.msn_list_delete_error_id),
                KAlertDialog.ERROR_TYPE)
        }
    }


    /**
     * borra el producto de la lista en firebase
     */
    fun deleteListInFirebase(){
        val idListForUserInFirebase = sharedPref.getValueString(Constant.KEYID_LIST_FOR_USER_FIREBASE)

        scope.async() {

            val collectionReference  = firebaseFirestore!!.collection(AppConstants.FB_COLECTION_LIST_FOR_USER)
                .document(idListForUserInFirebase!!)
                .delete()
                .addOnSuccessListener {
                    Log.d(Constant.TAG_DELETE_LIST_ON_FIREBASE, "$MSN_DELETE_TAG_SUCCESS_IN_FIREBASE $idListForUserInFirebase")
                }
                .addOnFailureListener {
                        e -> Log.w(Constant.TAG_DELETE_LIST_ON_FIREBASE, Constant.MSN_DELETE_TAG_ERROR_IN_FIREBASE, e)
                }
        }
    }


    /*
     *  Guarda una nueva lista
     */
    fun saveNewList(nameList: String) {
        val listDB = ListDB(nameList, Utils.getDateCurrent(), true, false, 0F, 0)
        runBlocking {
            processSaveNewList(listDB, nameList)
        }
    }


    //Funcion suspend para insertar una nueva lista
    suspend fun insertList(listDB: ListDB) {
        Log.v(Constant.TAG_PROCESS_INSERT_LIST, "insertList ")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryList.insertList(listDB)
    }


    //Funcion suspend para almacenar el nombre de la lista actual
    suspend fun saveNameListCurrent(nameList: String) {
        Log.v(Constant.TAG_PROCESS_INSERT_LIST, "saveNameListCurrent ")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        sharedPref.saveValueString(Constant.KEY_NAME_LIST_CURRENT , nameList)
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processSaveNewList(listDB: ListDB, nameList: String) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_PRODUCT_LIST_SHOP)
        launch {
            insertList(listDB)
            saveNameListCurrent(nameList)
            val user = getLoginUser()
            sendListToFirebase(user)
            getLastListForStatusOpened()
            checkListOpenDB()
        }
    }


    //Funcion suspend para obtener el usuario login actual
    suspend fun getLoginUser(): UserDB{
        Log.v(Constant.TAG_PROCESS_INSERT_LIST, "getLoginUser ")
        return repositoryUser.getLoginUserWithNickName()!!
    }


    //Funcion suspend para guardar la lista en Firebase
    suspend fun sendListToFirebase(usersDb: UserDB){
        Log.v(Constant.TAG_PROCESS_INSERT_LIST, "sendListToFirebase ")
        val collectionReference = this.firebaseFirestore!!.collection(AppConstants.FB_COLECTION_LIST_FOR_USER)
        val nameListCurrent = sharedPref.getValueString(Constant.KEY_NAME_LIST_CURRENT) ?: ""
        val nameUserCurrent = usersDb?.nickname ?: ""

        if(!nameListCurrent.isEmpty() && !nameUserCurrent.isEmpty()){

            val docData = hashMapOf(
                "date_creation" to Utils.getDateCurrent(),
                "name_list" to nameListCurrent,
                "username" to nameUserCurrent)

            collectionReference.add(docData)
                .addOnSuccessListener {
                    Log.v("ID lista en Firebase", it.id);
                    sharedPref.saveValueString(Constant.KEYID_LIST_FOR_USER_FIREBASE, it.id)

                    Utils.displayConfirmation(_context,"",
                        _context.getString(R.string.msn_list_sinchronized_success_firebase),
                        KAlertDialog.SUCCESS_TYPE)
                }
                .addOnFailureListener { e ->
                    Utils.displayConfirmation(_context,
                        _context.getString(R.string.msn_list_sinchronized_error_firebase),
                        "",
                        KAlertDialog.ERROR_TYPE)
                }
        }
    }


    //Funcion suspend para obtener la lista abierta actual
    suspend fun getLastListForStatusOpened(){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        _openListLiveData = repositoryList.getLastListForStatusOpened(true)
        Log.v(Constant.TAG_PROCESS_INSERT_LIST, "getLastListForStatusOpened ")
    }

    //Funcion suspend para comprobar si se tiene una lista abierta
    suspend  fun checkListOpenDB() {
        Log.v(Constant.TAG_PROCESS_INSERT_LIST, "checkListOpenDB ")
        _openListLiveData.let {

            //si se tiene una lista con estado abierta
            if(_openListLiveData!!.size > 0 ){

                val isOpenList = _openListLiveData!![0].isOpen
                if(!isOpenList){
                    Log.v("Lista ", "la lista No esta abierta")
                    addListConfirmation()
                }
                else{
                    Log.v("Lista ", "la lista SI esta abierta")
                    val idListCurrent = _openListLiveData!![0].id
                    sharedPref.saveValueInt(Constant.KEYID_LIST_CURRENT , idListCurrent)
                    sharedPref.saveValueBoolean(Constant.KEY_LIST_ISOPENED, true)
                }
            }
        }
    }


    /**
     *  Obtiene los datos para la creacion del nuevo producto en Firebase
     */
    fun getDataForCreateProductOnFirebase() {

            var  mAlertDialog: AlertDialog? = null
            val builder = AlertDialog.Builder(_context)
            val inflater = LayoutInflater.from(_context)

            val dialogLayout = inflater.inflate(R.layout.alert_create_new_product, null)
            builder.setView(dialogLayout)

            val btnAdd = dialogLayout.findViewById<Button>(R.id.btn_create_product)
            val imgClose =  dialogLayout.findViewById<ImageView>(R.id.img_close_create_product)

            btnAdd.setOnClickListener {
                val nameProductForFirebase= dialogLayout.findViewById<EditText>(R.id.edtxt_name_product)
                val priceProductForFirebase= dialogLayout.findViewById<EditText>(R.id.edtxt_price)
                val descriptionForFirebase= dialogLayout.findViewById<EditText>(R.id.edtxt_descrip)

                if(!nameProductForFirebase.text.toString().isEmpty()){

                    var price: Float = 0F
                    if(!priceProductForFirebase.text.toString().isEmpty()){
                        price = priceProductForFirebase.text.toString().toFloat()
                    }

                    this.createProductOnFirebase(nameProductForFirebase.text.toString(),
                        price,
                        descriptionForFirebase.text.toString())
                    mAlertDialog!!.dismiss()
                }
                else{
                    Toast.makeText(_context,
                        _context.getString(R.string.title_warning_product_fields_empty),
                        Toast.LENGTH_LONG)
                        .show()
                }
            }

            imgClose.setOnClickListener {
                mAlertDialog!!.dismiss()
            }

            mAlertDialog = builder.show()
    }


    /**
     * metodo que crea un nuevo producto ante firebase
     */
    fun createProductOnFirebase(name: String, price: Float, description: String){
        val idCategoryForFirebase=  _idCategory
        //Aumenta en uno, el proximo ID que se asignara a este producto
        val idProductForFirebase    = (lastNumberIDProducto+1).toString()

        //val firebaseFirestore = FirebaseFirestore.getInstance()
        val collectionReference = this.firebaseFirestore!!.collection(AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES)


        val docData = hashMapOf(
            "idCategory" to idCategoryForFirebase,
            "idProduct" to idProductForFirebase,
            "image" to AppConstants.URL_IMAGE_PRODUCT_NOW_DEFAUL,
            "name" to name,
            "price" to price,
            "description" to description
        )

        collectionReference.add(docData)
            .addOnSuccessListener {

                //actualiza la lista de producto desde Firebase
                _updateListProductFirebase.postValue(true)

                Utils.displayConfirmation(_context,
                    _context.getString(R.string.title_create_product_success_firebase),
                    "",
                    KAlertDialog.SUCCESS_TYPE)
            }
            .addOnFailureListener { e ->
                Utils.displayConfirmation(_context,
                    _context.getString(R.string.title_create_product_error_firebase),
                    "",
                    KAlertDialog.ERROR_TYPE)
            }
    }


    /**
     * Envia la lista de productos a Firebase
     */
    fun sendListDetailProductForUser(){
        runBlocking {
            processSendListDetailProductForUser()
        }
    }


    //Funcion suspend para actualiza los datos de la lista tras haber finalizado de editarla
    suspend fun getUpdateValuesForList(): ListDB {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "getUpdateValuesForList ")
        delay(Constant.TIME_DELAY_PROCESS_APP)

        val listProduct= repositoryProdListShop.getAllProductOfList(idListCurrent)
        if(_openListLiveData != null && _openListLiveData!!.size > 0){
            val listOpenCurrent  = _openListLiveData!![0]

            var valueTotalShop = 0F;
            for (product in listProduct){
                valueTotalShop += product.price * product.cant
            }

            listOpenCurrent.value         = valueTotalShop
            listOpenCurrent.countProducts = listProduct.size

            return listOpenCurrent
        }

        return ListDB()
    }


    //Funcion suspend para actualiza los datos de la lista tras haber finalizado de editarla
    suspend fun updateListValue(list: ListDB, countTotalProduct: Int) {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "updateListValue ")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryList.updateListValues(list.id, list.isFavorite, list.value, countTotalProduct, false)
    }


    //Funcion suspend para obtener todos los producto de la lista
    suspend fun getCountProductAsociateToList(): Int {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "getCountProductAsociateToList")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        var count = 0
        val listAllProduct = repositoryProdListShop.getAllProductOfList(idListCurrent)
        for(product in listAllProduct){
            count += product.cant
        }

        return count
    }


    //Funcion suspend para actualizar los datos de la lista tras haber finalizado de editarla
    suspend fun getAllProductOfListWithoutSinchronized(): MutableList<ProductListDB> {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "getAllProductOfListWithoutSinchronized")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        val listProduct
                = repositoryProdListShop.getAllProductOfListWithoutSinchronized(idListCurrent, true)
        return listProduct
    }


    //Funcion suspend para actualizar o agregar productos a Firebase
    suspend fun addOrUpdateProductInFirebase(listProduct: MutableList<ProductListDB>) {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "addOrUpdateProductInFirebase")
        delay(Constant.TIME_DELAY_PROCESS_APP)

        if(listProduct.size > 0){
            val refList
                    = firebaseFirestore!!.collection(AppConstants.FB_COLECTION_LIST_DETAIL_PRODUCT_FOR_USER)

            for (product in listProduct){
                //Si existe, realiza la actualizacion
                if(!product.IdFirebase.isEmpty()){
                    val refProdInListFirebase
                            = firebaseFirestore!!.collection(
                        AppConstants.FB_COLECTION_LIST_DETAIL_PRODUCT_FOR_USER).document(product.IdFirebase)

                    refProdInListFirebase.get()
                        .addOnSuccessListener {

                            if (it.exists()) {
                                updateProductInListFirebase(product, refProdInListFirebase)

                            } else {
                                Log.v(Constant.TAG_APP, "Id Documento no encontrado para actualizarlo")
                            }
                        }
                        .addOnFailureListener {e ->
                            Utils.displayConfirmation(_context,
                                _context.getString(R.string.msn_product_reference_error_firebase),
                                e.localizedMessage,
                                KAlertDialog.ERROR_TYPE)
                        }
                }
                //Si no existe se entiende que es un registro nuevo
                else{
                    addNewProductToListFirebase(product, refList)
                }
            }
        }
    }


    //Funcion suspend para cambiar el flag a lista cerrada
    suspend fun closeListCurrent() {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "closeListCurrent")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        //setea a cerrada la lista actual, despues de la confirmacion
        sharedPref.saveValueBoolean(Constant.KEY_LIST_ISOPENED, false)
        _navegationHome.postValue(true)
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processSendListDetailProductForUser() = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE)
        launch {
            getLastListForStatusOpened()
            val listDb = getUpdateValuesForList()
            if(listDb.id != null){
                val countTotalProducts = getCountProductAsociateToList()
                updateListValue(listDb, countTotalProducts)
                val list = getAllProductOfListWithoutSinchronized()
                addOrUpdateProductInFirebase(list)
                closeListCurrent()
            }
            else {
                Utils.displayConfirmation(_context, "Error", "Objecto de la lista no identificado, no se puede actualizar la lista", KAlertDialog.WARNING_TYPE)
            }

        }
    }


    /**
     * agrega un nuevo producto a la lista de productos de firebase
     */
    fun addNewProductToListFirebase(product: ProductListDB,referenceList: CollectionReference) {
        val idListForUserInFirebase = sharedPref.getValueString(Constant.KEYID_LIST_FOR_USER_FIREBASE)
        val docData = hashMapOf(
            "cant"          to product.cant,
            "idCategory"    to product.idCategory,
            "id_list_for_user" to idListForUserInFirebase,
            "measure"       to product.measure,
            "name"          to product.name,
            "nameCategory"  to fullNameCategory.value,
            "observ"        to product.observ,
            "price"         to product.price)

        referenceList.add(docData)
            .addOnSuccessListener {ItemFirebase ->
                Log.v("ID create in Firebase", ItemFirebase.id);

                //set Id of Firebase
                setIdFirebaseToProduct(product.id, ItemFirebase.id)

                //set a false the sinchronized to firebase
                updateStatusPendingToProduct(product.id, false)
            }
            .addOnFailureListener { e ->
                Utils.displayConfirmation(_context,
                    _context.getString(R.string.msn_product_sinchronized_error_firebase),
                    e.localizedMessage,
                    KAlertDialog.ERROR_TYPE)
            }
    }


    /**
     * acutualiza un nuevo producto en la lista de firebase
     */
    fun updateProductInListFirebase(product: ProductListDB, refDocument: DocumentReference) {
        val idListForUserInFirebase = sharedPref.getValueString(Constant.KEYID_LIST_FOR_USER_FIREBASE)

        refDocument.update("cant", product.cant);
        refDocument.update("measure", product.measure);
        refDocument.update("observ", product.observ)
        .addOnSuccessListener { ItemFirebase ->
            Toast.makeText(_context, "Producto Actualizado Correctamente", Toast.LENGTH_LONG).show()
        }
        .addOnFailureListener { e ->
            Utils.displayConfirmation(_context,
                _context.getString(R.string.msn_product_sinchronized_error_firebase),
                e.localizedMessage,
                KAlertDialog.ERROR_TYPE)
        }
    }

    /**
     * acutualiza el valor del producto en firebase
     */
    suspend fun updateValueProductFirebase(product: ProductListDB) {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "updateValueProductFirebase")
        delay(Constant.TIME_DELAY_PROCESS_APP)

            val IdFirebaseForProduct = getIDFirebaseForProduct(product!!.idProduct)

            if(IdFirebaseForProduct != null){
                val refProdInListFirebase
                        = firebaseFirestore!!.collection(
                    AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES).document(IdFirebaseForProduct)

                refProdInListFirebase.update("price", product.price)
                    .addOnSuccessListener { ItemFirebase ->
                        Log.v(product.name,"Valor del Producto Actualizado Correctamente con IdFirebase : $IdFirebaseForProduct")
                    }
                    .addOnFailureListener { e ->
                        Log.v(product.name,"Error actualizado valor del producto con IdFirebase : $IdFirebaseForProduct")
                    }
            }
            else{
                Log.v(Constant.TAG_APP, "No se pudo sincronizar el precioa ante firebase para ${product.name} con ID ${product.idProduct}")
            }
    }


    suspend fun getIDFirebaseForProduct(idProduct: Int): String{
        Log.v(Constant.TAG_PROCESS_GET_FIREBASE_FOR_PRODUCT, "getIDFirebaseForProduct")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        return productRepositoryDB.getIdFirebase(idProduct)
    }


    /**
     * Asigna el ID de Firebase al producto
     */
    fun setIdFirebaseToProduct(id: Int, idFirebase: String){
        scope.async {
            repositoryProdToList.updateIDFirebase(id, idFirebase)
        }
    }



    /**
     * Modifica el estado de sincronizacion de un producto, para tomar acciones ante Firebase
     */
    fun updateStatusPendingToProduct(id: Int, status: Boolean){
        scope.async {
            repositoryProdToList.updateSyncPending(id, status)
        }
    }
}