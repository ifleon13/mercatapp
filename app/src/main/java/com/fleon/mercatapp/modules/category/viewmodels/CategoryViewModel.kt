package com.fleon.mercatapp.modules.category.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.commons.database.category.CategoryDB
import com.fleon.mercatapp.commons.database.category.CategoryDBRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext
import com.fleon.mercatapp.commons.utils.AppConstants
import com.fleon.mercatapp.commons.utils.Jobs
import com.fleon.mercatapp.commons.utils.Utils
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch

class CategoryViewModel(application: Application) : AndroidViewModel(application) {

    private val repository   = CategoryDBRepository(application)

    // internal
    private val _visibilityProgressBar = MutableLiveData<Boolean>()
    //external
    val visibilityProgressBar: LiveData<Boolean>
        get() = _visibilityProgressBar

    // internal
    private val _fetchListCaterotyFromFirebase = MutableLiveData<Boolean>()
    //external
    val fetchListCaterotyFromFirebase: LiveData<Boolean>
        get() = _fetchListCaterotyFromFirebase

    val categoryListLiveData =  MutableLiveData<MutableList<CategoryDB>>()

    init {
        _fetchListCaterotyFromFirebase.postValue(false)
        //query the categories in database
        Utils.queryCategoriesDB(repository)
    }

    private val _navigateToProducts = MutableLiveData<CategoryDB>()
    val navigateToProducts
        get() = _navigateToProducts


    /**
     *
     */
    fun onCategoryClicked(category: CategoryDB){
        _navigateToProducts.value = category
    }


    /**
     *
     */
    fun getListCategoryRoom() {
        Jobs.get().launch {
            try {
                val categoryList = repository.getAllCategorys()
                if(categoryList != null && categoryList.size > 0){
                    categoryListLiveData.postValue(categoryList)
                    _visibilityProgressBar.postValue(false)
                }
                else{
                    //setting observer for get list from Firebase
                    _fetchListCaterotyFromFirebase.postValue(true)
                }
            } catch(e: Exception) {
                _visibilityProgressBar.postValue(false)
                println("Error : "+e.message.toString())
            }
        }

    }


    /**
     *
     */
    fun getListCategoryFirebase(firebaseFirestore: FirebaseFirestore?) {
        val collectionReference = firebaseFirestore!!.collection(AppConstants.FB_COLECTION_CATEGORY)
        val itemList: MutableList<CategoryDB> = ArrayList()

        collectionReference
            .orderBy(AppConstants.FB_COLECTION_CATEGORY_FIELD_NAME)
            .get()
            .addOnCompleteListener {
                for (category in it.result!!.iterator()){
                    Log.d("log", category.getString(AppConstants.FB_COLECTION_CATEGORY_FIELD_NAME))

                    val newCategory = CategoryDB()
                    newCategory.id    =  category.getString(AppConstants.FB_COLECTION_CATEGORY_FIELD_ID)!!
                    newCategory.image =  category.getString(AppConstants.FB_COLECTION_CATEGORY_FIELD_IMAGE)!!
                    newCategory.name  =  category.getString(AppConstants.FB_COLECTION_CATEGORY_FIELD_NAME)!!
                    newCategory.type  =  category.getString(AppConstants.FB_COLECTION_CATEGORY_FIELD_TYPE)!!

                    itemList.add(newCategory)
                }
                Log.d("log", "onComplete")
                categoryListLiveData.postValue(itemList)
                this.saveListCategories(itemList)
            }
    }




    fun saveListCategories(list: MutableList<CategoryDB>){

        Jobs.get().launch {
            for (category in list.iterator()){
                val cat = repository.getCategoryById(category.id)
                val idExist = cat.value?.id
                if(idExist == null){
                    repository.insertCategory(category)
                }
            }
        }
    }


    /**
     * setting to null when navigation is completed
     */
    fun onProductNavigated() {
        _navigateToProducts.value = null
    }


    /**
     *
     */
    fun setVisibleProgressBar(visible: Boolean) {
        _visibilityProgressBar.value = visible
    }
}