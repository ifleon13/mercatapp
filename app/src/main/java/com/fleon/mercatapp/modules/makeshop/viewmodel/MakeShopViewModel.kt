package com.fleon.mercatapp.modules.makeshop.view.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.list.ListRepositoryDB
import com.fleon.mercatapp.commons.database.product.ProductRepositoryDB
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.database.products_list.ProductListRepositoryDB
import com.fleon.mercatapp.commons.utils.AppConstants
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.Utils
import com.fleon.mercatapp.modules.makeshop.view.container.RecyclerViewContainer
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MakeShopViewModel(val ctx: Context, application: Application) : AndroidViewModel(application) {

    private val repositoryProdListShop = ProductListRepositoryDB(application)
    private val repositoryList         = ListRepositoryDB(application)
    private val firebaseFirestore      = FirebaseFirestore.getInstance()
    private val productRepositoryDB    =  ProductRepositoryDB(application)

    // internal
    private val _editProductMakeShop = MutableLiveData<RecyclerViewContainer>()
    //external
    val editProductMakeShop: LiveData<RecyclerViewContainer>
        get() = _editProductMakeShop


    // internal
    private val _updatePriceTemForProductMakeShop = MutableLiveData<RecyclerViewContainer>()
    //external
    val updatePriceTemForProductMakeShop: LiveData<RecyclerViewContainer>
        get() = _updatePriceTemForProductMakeShop

    var updateValueListShopTotalTemp = MutableLiveData<Float>()

    /**
     * Observer Metodo para informar el nuevo valor total de la compra
     */
    suspend fun updateTotalValueShopTemp(idList: Int) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        var value = 0.0
        val list = repositoryProdListShop.getAllProductOfList(idList)
        for (l in list){
            value += l.priceTemp
        }
        updateValueListShopTotalTemp.postValue(value.toFloat())
    }

    suspend fun getProductForListID(idList: Int): MutableList<ProductListDB>{
        delay(Constant.TIME_DELAY_PROCESS_APP)
        return repositoryProdListShop.getAllProductOfList(idList)
    }


    suspend fun updateAllPriceOfListShopVM(idList: Int, list: MutableList<ProductListDB>){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        for (pdt in list){
            repositoryProdListShop.updateAllPriceOfListShopRP(idList, pdt)
        }
    }


    fun editItem(recyclerProductItem: RecyclerViewContainer){
        _editProductMakeShop.postValue(recyclerProductItem)
    }


    fun updatePriceTempForProduct(itemProduct: RecyclerViewContainer){
        _updatePriceTemForProductMakeShop.postValue(itemProduct)
    }


    suspend fun updatePriceTempForProductVM(idList: Int, itemRecycler: RecyclerViewContainer){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryProdListShop.setPriceTemporalRP(idList, itemRecycler.product!!.idProduct, itemRecycler.product!!.priceTemp)
    }


    suspend fun updateDataProduct(cant: Int, measure: String, observ: String, idList: Int, idProduct: Int){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryProdListShop.updateDataProduct(idList, idProduct,  observ, cant, measure, false)
    }


    suspend fun updateCountProductForListVM(idList: Int){
        val countProducts = getCountProductAsociateToList(idList)
        repositoryList.updateCountProductForListRP(countProducts, idList)
    }



    suspend fun getValueTotalListVM(idList: Int):Float{
        delay(Constant.TIME_DELAY_PROCESS_APP)
        val listProducts = repositoryProdListShop.getAllProductOfList(idList)

        var valueTotalList = 0F
        for (product in listProducts){
            val totalProduct = product.price * product.cant
            valueTotalList += totalProduct
        }

        return valueTotalList
    }


    suspend fun updateValueTotalListVM(idList: Int, valueTotalList: Float){
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryList.updateValueTotalListRP(idList, valueTotalList)
    }




    //Funcion suspend para obtener todos los producto de la lista
    suspend fun getCountProductAsociateToList(idList: Int): Int {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "getCountProductAsociateToList")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        var count = 0
        val listAllProduct = repositoryProdListShop.getAllProductOfList(idList)
        for(product in listAllProduct){
            count += product.cant
        }

        return count
    }




    /**
     * acutualiza cada uno de los productos en firebase con el nuevo valor pagado en la compra
     */
   suspend fun updateValueAllProductFirebase(list: MutableList<ProductListDB>) {
        Log.v(Constant.TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE, "updateValueAllProductFirebase")
        delay(Constant.TIME_DELAY_PROCESS_APP)

        for (product in list){

            val IdFirebaseForProduct = getIDFirebaseForProduct(product!!.idProduct)

            val refProdInListFirebase
                    = firebaseFirestore!!.collection(
                AppConstants.FB_COLECTION_PRODUCTS_FOR_CATEGORIES).document(IdFirebaseForProduct)

                //val priceNew = Utils.getValorFormated(product.priceTemp, false)
                //val priceNew = product.priceTemp
                //val priceNewReplace  = priceNew.replace(",", ".")

                refProdInListFirebase.update("price", product.priceTemp)
                    .addOnSuccessListener { ItemFirebase ->
                        Log.v(product.name,"Valor del Producto Actualizado Correctamente con IdFirebase : $IdFirebaseForProduct")
                    }
                    .addOnFailureListener { e ->
                        Log.v(product.name,"Error actualizado valor del producto con IdFirebase : $IdFirebaseForProduct")
                    }
        }
    }




    suspend fun getIDFirebaseForProduct(idProduct: Int): String{
        Log.v(Constant.TAG_PROCESS_GET_FIREBASE_FOR_PRODUCT, "getIDFirebaseForProduct")
        delay(Constant.TIME_DELAY_PROCESS_APP)
        return productRepositoryDB.getIdFirebase(idProduct)
    }

}

