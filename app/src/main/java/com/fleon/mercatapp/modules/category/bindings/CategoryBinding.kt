package com.fleon.mercatapp.modules.category.bindings

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.category.CategoryDB
import com.squareup.picasso.Picasso


@BindingAdapter("categoryImagenView")
fun ImageView.setImage(item: CategoryDB?) {
    var urlImage = item?.image
    item?.let {
        //Picasso.get().load(urlImage).into(this)
        Picasso.get().load(urlImage).placeholder(R.drawable.progress_animation ).into(this)
    }
}


@BindingAdapter("categoryNameTextView")
fun TextView.setRatting(item: CategoryDB?) {
    item?.let {
        text = item?.name
    }
}