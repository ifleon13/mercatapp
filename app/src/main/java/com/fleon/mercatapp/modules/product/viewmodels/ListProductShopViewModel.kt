package com.fleon.mercatapp.modules.product.viewmodels

import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.database.products_list.ProductListRepositoryDB
import com.fleon.mercatapp.commons.utils.Constant
import com.fleon.mercatapp.commons.utils.SharedPreference
import com.fleon.mercatapp.commons.utils.Utils
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.*
import java.lang.Runnable
import kotlin.coroutines.CoroutineContext


class ListProductShopViewModel(val ctx: Context, application: Application) : AndroidViewModel(application) {

    private val _moviesJob = Job()
    private val coroutineContext: CoroutineContext
        get() = _moviesJob + Dispatchers.IO

    var productListLiveData           =  MutableLiveData<MutableList<ProductListDB>>()
    private val repositoryProdListShop = ProductListRepositoryDB(application)
    private val sharedPref: SharedPreference = SharedPreference(ctx)
    private var idListCurrent: Int     = 0;
    private var listOpened: Boolean    = false

    // internal
    private val _updateListProductShop = MutableLiveData<Boolean>()
    //external
    val updateListProductShop: LiveData<Boolean>
        get() = _updateListProductShop

    // internal
    private val _updateValueTotalShop = MutableLiveData<Boolean>()
    //external
    val updateValueTotalShop: LiveData<Boolean>
        get() = _updateValueTotalShop


    init {
        this.listOpened    = sharedPref.getValueBoolean(Constant.KEY_LIST_ISOPENED, false)
        this.idListCurrent = sharedPref.getValueInt(Constant.KEYID_LIST_CURRENT)
    }


    /**
     * Obtiene la lista de producto añadidos de la unica lista con estado abierta
     */
    fun getListOpenProductForShop(){
        this.listOpened    = sharedPref.getValueBoolean(Constant.KEY_LIST_ISOPENED, false)
        this.idListCurrent = sharedPref.getValueInt(Constant.KEYID_LIST_CURRENT)

        //si hay una lista actualmente abierta
        if(this.listOpened){
            if(this.idListCurrent > 0){
                //ejecuta el siguiente bloque en secuencia
                runBlocking {
                    processListOpenProductForShop(idListCurrent)
                }
            }
            else {
                Utils.displayConfirmation(ctx, ctx.getString(R.string.delete_list),
                    ctx.getString(R.string.title_error_query_list),
                    KAlertDialog.WARNING_TYPE)
            }
        }else{
            Log.i(Constant.TAG_APP, "No existe ninguna lista abierta actualmente.")
        }
    }


    /**
     * Funcion suspend para actualizar la lista de productos
     */
    suspend fun getListProductDB(idListCurrent: Int ): MutableList<ProductListDB> {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        val list = repositoryProdListShop.getAllProductOfList(idListCurrent)
        return list
    }


    /**
     * Funcion suspend para actualizar la lista de productos
     */
    suspend fun postValueTotalShop(list: MutableList<ProductListDB>) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        productListLiveData.postValue(list)
    }


    /**
     * Funcion suspend para actualizar al valor total de la compra
     */
    suspend fun updateValueTotalShop(value: Boolean) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        _updateValueTotalShop.postValue(value)
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processListOpenProductForShop(idListCurrent: Int) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_PRODUCT_LIST_SHOP)
        launch {
                    val list = getListProductDB(idListCurrent)
                    postValueTotalShop(list)
                    updateValueTotalShop(true)
        }
    }






    /**
     * Presenta el cuadro de confirmacion para la eliminacion del producto
     */
    fun deleteProductShopFromList(product: ProductListDB){
        val mHandler = Handler(Looper.getMainLooper())
        mHandler.post(Runnable {
            KAlertDialog(ctx, KAlertDialog.WARNING_TYPE)
                .setTitleText(ctx.getString(R.string.title_confirm_delete_product_from_list))
                .setContentText(ctx.getString(R.string.msn_confirm_delete_product_from_list))
                .setConfirmText(ctx.getString(R.string.title_continue))
                .setConfirmClickListener {
                        sDialog -> sDialog
                    .dismissWithAnimation()
                    //Ejecuta el siguiente bloque en secuencia
                    runBlocking {
                        processdeleteProductToList(product.id)
                    }
                }
                .setCancelText(ctx.getString(R.string.cancel))
                .setCancelClickListener {
                        sDialog -> sDialog
                    .dismissWithAnimation()
                }
                .show()
        })
    }



    /**
     * Funcion suspend para actualizar la lista de productos de la compra
     */
    suspend fun updateListProductShop(value: Boolean) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        _updateListProductShop.postValue(value)
    }


    /**
     * Funcion que elimina un producto de la lista de productos de la compra
     */
    suspend fun deleteProductToList(idProdcut: Int) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        val list = repositoryProdListShop.deleteProductToList(idProdcut)
    }


    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processdeleteProductToList(idProdcut: Int) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_DELETE_OF_LIST_SHOP)
        launch {
            updateListProductShop(false)
            deleteProductToList(idProdcut)
            updateListProductShop(true)
        }
    }




    /*
     *  muestra la ventana para editar un producto seleccionado
     */
    fun updateProductConfirmation(product: ProductListDB) {

            var  mAlertDialog: AlertDialog? = null
            val builder = AlertDialog.Builder(ctx)
            val inflater = LayoutInflater.from(ctx)

            val dialogLayout =
                inflater.inflate(R.layout.alert_dialog_setting_buy, null)
            builder.setView(dialogLayout)

            val btnAdd = dialogLayout.findViewById<Button>(R.id.btn_add)
            btnAdd.text = ctx.getString(R.string.title_btn_modified_product)


            val imgClose =  dialogLayout.findViewById<ImageView>(R.id.img_close)
            val txtvNameProduct  = dialogLayout.findViewById<TextView>(R.id.txtv_title_name_product)
            val txtvNameDescrip  = dialogLayout.findViewById<TextView>(R.id.txtv_descrip_product)

            val fieldObserv  = dialogLayout.findViewById<EditText>(R.id.edtxt_observ)
            val fieldCanti   = dialogLayout.findViewById<Spinner>(R.id.spn_canti)
            val fieldMeasu   = dialogLayout.findViewById<Spinner>(R.id.spn_measurements)
            txtvNameProduct.text      = product.name

            fieldObserv.setText(product.observ)
            fieldCanti.setSelection(product.cant-1)
            fieldMeasu.setSelection(Utils.getIndexMeasures(ctx, product.measure))

        btnAdd.setOnClickListener {
                
                val canti     = fieldCanti.getSelectedItem().toString().toInt()
                val measure = fieldMeasu.getSelectedItem().toString()
                val observ  = fieldObserv.text.toString()

                mAlertDialog!!.dismiss()
                updateProduct(product, observ, canti, measure)
            }

            imgClose.setOnClickListener {
                mAlertDialog!!.dismiss()
            }

            mAlertDialog = builder.show()
    }



    /**
     * Modifica las caracteristicas del producto agregado a la compra
     */
    fun updateProduct(product: ProductListDB, obsv: String, cant: Int, measure: String){

        //si algunos de los datos del producto es modificado
        if(!product.observ.equals(obsv)
            || product.cant != cant
            || !product.measure.equals(measure)){

            runBlocking {
                processUpdateDataProduct(product, obsv, cant, measure)
            }
        }
    }



    /**
     * Funcion que actualiza los datos del producto
     */
    suspend fun updateDataProduct(product: ProductListDB, obsv: String, cant: Int, measure: String) {
        delay(Constant.TIME_DELAY_PROCESS_APP)
        repositoryProdListShop.updateDataProduct(this.idListCurrent, product.id, obsv, cant, measure, true)
    }



    /**
     * Funcion suspend que ejecuta procesos secuanciales
     */
    suspend fun processUpdateDataProduct(product: ProductListDB, obsv: String, cant: Int, measure: String) = withContext(Dispatchers.IO) {
        Log.v(Constant.TAG_APP, Constant.TAG_PROCESS_UPDATE_PRODUCT_OF_LIST_SHOP)
        launch {
            updateDataProduct(product, obsv, cant, measure)
            updateListProductShop(true)
        }
    }
}



