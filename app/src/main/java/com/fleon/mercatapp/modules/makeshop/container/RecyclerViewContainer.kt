package com.fleon.mercatapp.modules.makeshop.view.container

import com.fleon.mercatapp.commons.database.products_list.ProductListDB

class RecyclerViewContainer (var product: ProductListDB?, var isHeader: Boolean, var headerTitle: String?)