package com.fleon.mercatapp.modules.list.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.databinding.SingleListHistoryItemBinding

class ListAdapter(val clickListener: ListListener): RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    var allList: MutableList<ListDB>  = ArrayList()

    fun listAdapter(list : MutableList<ListDB>){
        this.allList = list
    }


    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val item = allList.get(position)
        holder.bind(item, clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder.from(parent)
    }


    override fun getItemCount(): Int {
        return allList.size
    }



    class ListViewHolder private constructor(val binding: SingleListHistoryItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(list: ListDB, clickListener: ListListener){
            binding.list = list
            binding.executePendingBindings()
            binding.clickListenerList = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ListViewHolder {
                val lyInflater = LayoutInflater.from(parent.context)
                val binding = SingleListHistoryItemBinding.inflate(lyInflater, parent, false)
                return ListViewHolder(binding)
            }
        }
    }


    fun updateList(newList: List<ListDB>) {

        val diffCallback = ListDiffCallback(allList, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        allList.clear()
        allList.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }


    fun removeList(row : Int) {
        allList.removeAt(row)
        notifyItemRemoved(row)
    }
}

/**
 * Esta clase verifica los elementos para ver si son del mismo tipo o tienen los mismos valores
 */
class ListDiffCallback(private val oldList: List<ListDB>,
                         private val newList: List<ListDB>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    //verifica el tipo de fila
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] === newList.get(newItemPosition)
    }

    //verifica para ver si las filas son iguales
    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition] == newList[newPosition]
    }
}


/**
 * Listener de eventos para el cardView
 */
class ListListener(val clickListener: (list: ListDB, type: TYPE_ACTION) -> Unit) {
    fun onClickList(list: ListDB, type: TYPE_ACTION) = clickListener(list, type)
}


enum class TYPE_ACTION {
    DELETE,
    SELECT,
    EDIT
}