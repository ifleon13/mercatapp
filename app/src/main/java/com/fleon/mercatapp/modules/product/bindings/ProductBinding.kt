package com.fleon.mercatapp.modules.product.bindings

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.product.ProductDB
import com.squareup.picasso.Picasso
import org.fabiomsr.moneytextview.MoneyTextView

@BindingAdapter("productImagenView")
fun ImageView.setImage(item: ProductDB?) {
    var urlImage = item?.image
    item?.let {
        //Picasso.get().load(urlImage).into(this)
        Picasso.get().load(urlImage).placeholder(R.drawable.progress_animation ).into(this)
    }
}


@BindingAdapter("productNameTextView")
fun TextView.setRatting(item: ProductDB?) {
    item?.let {
        text = item?.name
    }
}


@BindingAdapter("productDescription")
fun TextView.setProductDescription(item: ProductDB?) {
    item?.let {
        text = item?.description
    }
}


@BindingAdapter("productPriceTextView")
fun setPrice(view: MoneyTextView, item: ProductDB?) {
    item?.price.let {
        view.amount = item!!.price
    }
}