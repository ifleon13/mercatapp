package com.fleon.mercatapp.modules.category.views


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.category.CategoryDB
import com.fleon.mercatapp.databinding.FragmentCreateBinding
import com.fleon.mercatapp.modules.category.adapters.CategoryAdapter
import com.fleon.mercatapp.modules.category.adapters.CategoryListener
import com.fleon.mercatapp.modules.category.viewmodels.CategoryViewModel
import com.google.firebase.firestore.FirebaseFirestore
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_create.*


/**
 * A simple [Fragment] subclass.
 *
 */
class CreateFragment : Fragment() {

    private lateinit var binding: FragmentCreateBinding
    private lateinit var categoryViewModel: CategoryViewModel
    private var firebaseFirestore: FirebaseFirestore? = null
    private var cAdapter: CategoryAdapter? = null
    private var listCategorys: MutableList<CategoryDB> = ArrayList()
    private var isRefreshLayout: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //
        this.initComponents(inflater, container)

        //
        this.getListCategory()

        //
        this.settingFieldObservables()

        //
        return this.binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //
        this.swipeRefreshCategories.setOnRefreshListener {
            this.isRefreshLayout = true
            this.getListCategory()
        }
    }


    private fun initComponents(inflater: LayoutInflater, container: ViewGroup?){

        this.firebaseFirestore = FirebaseFirestore.getInstance()

        this.binding = DataBindingUtil.inflate(inflater,
            com.fleon.mercatapp.R.layout.fragment_create, container, false)


        this.binding.lifecycleOwner = this


        this.categoryViewModel  = ViewModelProviders.of(this).get(CategoryViewModel::class.java)


        //asigna al viewmodel definido en el data del xml, el viewmodel creado
        this.binding.viewModelCat = this.categoryViewModel


        //configura el adapter pasandole el listener para eventos sobre cada Item
        this.cAdapter = CategoryAdapter(CategoryListener { category ->
            this.categoryViewModel.onCategoryClicked(category)
        })
    }



    private fun getListCategory(){

        if(this.isRefreshLayout){
            this.swipeRefreshCategories.isRefreshing = true
        }

        //
        this.categoryViewModel.getListCategoryRoom()
    }


    /**
     * Configura los campos observables
     */
    fun settingFieldObservables(){

        this.categoryViewModel.visibilityProgressBar.observe(this, Observer {visibilibity: Boolean ->
            if(visibilibity){
                this.binding.progressBarCategory.visibility = View.VISIBLE
            }
            else {
                this.binding.progressBarCategory.visibility = View.GONE
            }
        })


        this.categoryViewModel.fetchListCaterotyFromFirebase.observe(this, Observer {fetchFromFirebase: Boolean ->
            if(fetchFromFirebase != null && fetchFromFirebase){
                this.categoryViewModel.getListCategoryFirebase(this.firebaseFirestore)
            }
        })


        this.categoryViewModel.navigateToProducts.observe(this, Observer { category ->
            category?.let {
                this.findNavController()?.navigate(CreateFragmentDirections.actionCreateFragmentToProductFragment(category.id))
                this.categoryViewModel.onProductNavigated()

            }
        })


        this.categoryViewModel.categoryListLiveData.observe(this, Observer {
            //TODO - Your Update UI Logic
            this.listCategorys.clear()
            this.listCategorys.addAll(it)

            //--------
            this.cAdapter?.updatePostingCategorys(it)
            this.binding.recyclerCategory.adapter = this.cAdapter
            //--------

            this.categoryViewModel.setVisibleProgressBar(false)

            if(this.isRefreshLayout){
                this.swipeRefreshCategories.isRefreshing = false
                this.isRefreshLayout = false
            }
        })
    }

}
