package com.fleon.mercatapp.modules.product.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.databinding.SingleProductShopItemBinding


class ListProductShopAdapter(val clickListener: ProductShopListener): RecyclerView.Adapter<ListProductShopAdapter.ProductShopViewHolder>() {
    var allListProductsShop: MutableList<ProductListDB>  = ArrayList()


    override fun onBindViewHolder(holder: ProductShopViewHolder, position: Int) {
        val item = allListProductsShop.get(position)
        holder.bind(item, clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductShopViewHolder {
        return ProductShopViewHolder.from(
            parent
        )
    }


    override fun getItemCount(): Int {
        return allListProductsShop.size
    }


    class ProductShopViewHolder private constructor(val binding: SingleProductShopItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(productoShop: ProductListDB, clickListener: ProductShopListener){
            binding.pts = productoShop
            binding.executePendingBindings()
            binding.clickListenerps = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ProductShopViewHolder {
                val lyInflater = LayoutInflater.from(parent.context)
                val binding = SingleProductShopItemBinding.inflate(lyInflater, parent, false)
                return ProductShopViewHolder(
                    binding
                )
            }
        }
    }


    fun updatePostingProductShop(newPostingProductShops: List<ProductListDB>) {

        val diffCallback = ProductShopDiffCallback(allListProductsShop, newPostingProductShops)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        allListProductsShop.clear()
        allListProductsShop.addAll(newPostingProductShops)
        diffResult.dispatchUpdatesTo(this)

    }


    fun removeProductShop(row : Int) {
        allListProductsShop.removeAt(row)
        notifyItemRemoved(row)
    }


    fun addProductShop(row : Int, product: ProductListDB) {
        allListProductsShop.add(product)
        notifyItemInserted(row)
    }

    /**
     * Esta clase verifica los elementos para ver si son del mismo tipo o tienen los mismos valores
     */
    class ProductShopDiffCallback(private val oldList: List<ProductListDB>,
                              private val newList: List<ProductListDB>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        //verifica el tipo de fila
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] === newList.get(newItemPosition)
        }

        //verifica para ver si las filas son iguales
        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return oldList[oldPosition] == newList[newPosition]
        }
    }
}


/**
 * Listener de eventos para el item del producto y el icono delete
 */
class ProductShopListener(val clickListener: (product: ProductListDB, isEditing: Boolean) -> Unit) {
    fun onClickProductShop(product: ProductListDB, isEditing: Boolean) = clickListener(product, isEditing)
}
