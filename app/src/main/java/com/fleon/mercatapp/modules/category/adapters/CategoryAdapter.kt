package com.fleon.mercatapp.modules.category.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fleon.mercatapp.commons.database.category.CategoryDB
import com.fleon.mercatapp.databinding.SingleCategoryItemBinding


class CategoryAdapter(val clickListener: CategoryListener): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    var allListCategories: MutableList<CategoryDB>  = ArrayList()


    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val item = allListCategories.get(position)
        holder.bind(item, clickListener)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder.from(parent)
    }


    override fun getItemCount(): Int {
        return allListCategories.size
    }


    class CategoryViewHolder private constructor(val binding: SingleCategoryItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(category: CategoryDB, clickListener: CategoryListener){
            binding.ct = category
            binding.executePendingBindings()
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): CategoryViewHolder {
                val lyInflater = LayoutInflater.from(parent.context)
                val binding = SingleCategoryItemBinding.inflate(lyInflater, parent, false)
                return CategoryViewHolder(binding)
            }
        }
    }


    fun updatePostingCategorys(newpostingCategory: List<CategoryDB>) {

        val diffCallback = CategoryDiffCallback(allListCategories, newpostingCategory)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        allListCategories.clear()
        allListCategories.addAll(newpostingCategory)
        diffResult.dispatchUpdatesTo(this)

    }


    fun removeCategories(row : Int) {
        allListCategories.removeAt(row)
        notifyItemRemoved(row)
    }


    fun addCategories(row : Int, category: CategoryDB) {
        allListCategories.add(category)
        notifyItemInserted(row)
    }
}


/**
 * Esta clase verifica los elementos para ver si son del mismo tipo o tienen los mismos valores
 */
class CategoryDiffCallback(private val oldList: List<CategoryDB>,
                         private val newList: List<CategoryDB>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    //verifica el tipo de fila
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] === newList.get(newItemPosition)
    }

    //verifica para ver si las filas son iguales
    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition] == newList[newPosition]
    }
}


/**
 * Listener de eventos para el cardView
 */
class CategoryListener(val clickListener: (category: CategoryDB) -> Unit) {
    fun onClickCategory(category: CategoryDB) = clickListener(category)
}