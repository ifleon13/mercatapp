package com.fleon.mercatapp.commons.database.product

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface ProductDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(productDB: ProductDB): Long


    @Query("SELECT idChildFirebase FROM PRODUCT WHERE id = :idProduct ORDER BY id DESC LIMIT 1")
    fun getIDFirebase(idProduct: Int): String

}