package com.fleon.mercatapp.commons.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.fleon.mercatapp.R
import com.fleon.mercatapp.commons.database.category.CategoryDB
import com.fleon.mercatapp.commons.database.category.CategoryDBRepository
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.commons.database.list.ListRepositoryDB
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.database.products_list.ProductListRepositoryDB
import com.fleon.mercatapp.modules.makeshop.view.container.RecyclerViewContainer
import com.google.android.material.snackbar.Snackbar
import com.kinda.alert.KAlertDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext

class Utils {
    companion object {

        private var listProductToList: MutableList<ProductListDB> = ArrayList()
        private var listCategories: MutableList<CategoryDB> = ArrayList()
        private var listBd: MutableList<ListDB> = ArrayList()

        /**
         *  Obtiene el listado de categorias desde ROOM
         */
        fun queryCategoriesDB(repository: CategoryDBRepository){
            Jobs.get().launch {
                listCategories = repository.getAllCategorys()

                for (category in listCategories.iterator()){
                    Log.v("CATEGORY: ", category.name)
                }
            }
        }


        /**
         * Obtiene el lista de productos que han sido añadidos a una lista desde ROOM
         */
        fun queryListProductToList(repository: ProductListRepositoryDB, idList: Int){
            Jobs.get().launch {
                listProductToList = repository.getAllProductOfList(idList)

                for (list in listProductToList.iterator()){
                    Log.v("PRODUCT TO LIST: ", list.idList.toString()+ " "+
                            list.idProduct.toString()+ " "+
                            list.cant.toString()+ " "+
                            list.measure+ " "+
                            list.observ)
                }
            }
        }


        /**
         * Muestra un cuadro de informacion al usuario
         */
        fun displayConfirmation(context: Context, title: String, msn: String, typeAlert: Int){
            val mHandler = Handler(Looper.getMainLooper())
            mHandler.post(Runnable {
                KAlertDialog(context, typeAlert)
                    .setTitleText(title)
                    .setContentText(msn)
                    .setConfirmText(context.getString(R.string.title_continue))
                    .setConfirmClickListener {
                            sDialog -> sDialog
                        .dismissWithAnimation()
                    }
                    .show()
            })
        }


        fun getDateCurrent(): String{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val current = LocalDateTime.now()
                val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss")
                return current.format(formatter)
            }
                var date = Date();
                val formatter = SimpleDateFormat("MMM dd yyyy HH:mma")
               return formatter.format(date)
        }


        fun getDataFornameList():String{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val current = LocalDateTime.now()
                val formatter = DateTimeFormatter.ofPattern("EEEE, MMM dd, yyyy HH:mm a")
                return current.format(formatter)
            }
            var date = Date();
            val formatter = SimpleDateFormat("E, dd MMM yyyy hh:mm")
            return formatter.format(date)
        }


        fun isInternetOn(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
            val activeNetwork = cm?.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }


        fun getIndexMeasures(ctx: Context, itemToSearch: String): Int {
            val values = ctx.resources.getStringArray(R.array.measurements_values)
            return values.indexOf(itemToSearch) ?: 0
        }


        /**
         *
         */
        fun getIndexElementInListOfProductListDB(elementToSearch: RecyclerViewContainer, listElements: MutableList<RecyclerViewContainer>):Int{
            listElements.forEachIndexed { index, element ->
                if(elementToSearch.product!!.name.equals(element.product!!.name) &&
                    (elementToSearch.product!!.idCategory.toInt() == element.product!!.idCategory.toInt()) && !element.isHeader){
                    return index
                }
            }
            return -1
        }


        fun isRemoveSession(elementToSearch: RecyclerViewContainer, listElements: MutableList<RecyclerViewContainer>):Boolean{
            var countItemForID = 0
            //cantidad minima de informacion (sesion + item)
            val minItemMoreSession = 2
            listElements.forEachIndexed { index, element ->
                if(elementToSearch.product!!.idCategory.toInt() == element.product!!.idCategory.toInt()){
                    countItemForID += 1
                }
            }

            if(countItemForID == minItemMoreSession){
                return true
            }
            return false
        }


        fun getValorFormated(value: Double, withCoin: Boolean = true): String {
            var valueText = ""
            val characters = value?.toString().length

            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            df.format(value)

            //si el valor, tiene mas de un caracter, es decir se puede hacer un split
            if(characters > 1){
                val partsNonRegex = value.toString().split(".")

                if(partsNonRegex.get(1).length == 1 && partsNonRegex.get(1).toInt() != 0){
                    valueText = if (withCoin) "${df.format(value)}0 €" else "${df.format(value)}0"
                }
                else {
                    valueText = if (withCoin) "${df.format(value)} €" else "${df.format(value)}"
                }
                }
                else{
                valueText = if (withCoin) "${df.format(value)} €" else "${df.format(value)}"
                }
         return  valueText
        }
    }
}
