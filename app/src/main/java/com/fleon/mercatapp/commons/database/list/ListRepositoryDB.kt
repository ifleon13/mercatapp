package com.fleon.mercatapp.commons.database.list

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import com.fleon.mercatapp.commons.database.MercatDatabase
import com.fleon.mercatapp.commons.utils.Constant


class ListRepositoryDB(val application: Application) {
    private val listDAO: ListDAO? = MercatDatabase.getInstance(application)?.listDAO()


    //Obtiene el listado de "listas" con un tipo de estado y ordenadas descendentemente por ID
    fun getListForIdWithStatus(isOpen: Boolean): MutableList<ListDB> {
        val _sql = "SELECT * FROM LIST_SHOP WHERE isOpen = $isOpen ORDER BY id DESC"
        Log.v(Constant.QUERY_ROOM, _sql)
        return listDAO?.getListOrderForIdWithStatus(isOpen)!!
    }

    //Obtiene una lista por estado (Open ot Close)
    fun getLastListForStatusOpened(isOpen: Boolean): MutableList<ListDB> {
        val _sql = "SELECT * FROM LIST_SHOP WHERE isOpen = $isOpen ORDER BY id DESC LIMIT 1"
        Log.v(Constant.QUERY_ROOM, _sql)
        return listDAO?.getLastListForStatusOpened(isOpen)!!
    }


    //Inserta una lista
    fun insertList(listDB: ListDB) {
        if (listDAO != null) {
            InsertAsyncTask(listDAO, application).execute(listDB)
        }
    }


    //Elimina la lista
    fun deleteList(idList: Int) {
        val _sql = "DELETE FROM LIST_SHOP WHERE id = $idList"
        Log.v(Constant.QUERY_ROOM, _sql)
        return listDAO?.deleteList(idList)!!
    }


    //Re-abre una lista para poder editarla
    fun reOpenList(idList: Int, isOPen: Boolean) {
        val _sql = "UPDATE LIST_SHOP SET isOpen = $isOPen  WHERE id = $idList"
        Log.v(Constant.QUERY_ROOM, _sql)

        return listDAO?.reOpenList(idList, isOPen)!!
    }


    //Update los datos de un lista
    fun updateListValues(id: Int, isFavorite: Boolean, value: Float, countProducts: Int, isOpen: Boolean) {
        val _sql = "UPDATE LIST_SHOP SET isFavorite =$isFavorite, value =$value, countProducts =$countProducts, isOpen = $isOpen  WHERE id = $id"
        Log.v(Constant.QUERY_ROOM, _sql)
        return listDAO?.updateListValues(id, isFavorite, value, countProducts, isOpen)!!
    }


    //Update la cantidad de valores de la lista
    fun updateCountProductForListRP(countProducts: Int, idList: Int) {
        val _sql = "UPDATE LIST_SHOP SET countProducts =$countProducts WHERE id = $idList"
        Log.v(Constant.QUERY_ROOM, _sql)
        return listDAO?.updateCountProductForListDao(countProducts, idList)!!
    }


    //Update el valor total de la lista
    fun updateValueTotalListRP(idList: Int, valueTotal: Float) {
        val _sql = "UPDATE LIST_SHOP SET value = $valueTotal  WHERE id = $idList"
        Log.v(Constant.QUERY_ROOM, _sql)
        return listDAO?.updateValueTotalListDAO(idList, valueTotal)!!
    }


    private class InsertAsyncTask(private val listDAO: ListDAO, val application: Application) :
        AsyncTask<ListDB, Void, Void>() {
        override fun doInBackground(vararg listDb: ListDB?): Void? {
            for (newList in listDb) {
                if (newList != null) {
                    var newId = listDAO.insertList(newList)

                    if(newId != null){
                        Log.v("AÑADIR", "Lista Añadida Exitosamente con ID "+newId.toString())
                    }
                }
            }
            return null
        }
    }
}