package com.fleon.mercatapp.commons.database.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CategoryDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(categoryDB: CategoryDB)


    @Query("SELECT * FROM CATEGORY ORDER BY name")
    fun getListCategoryOrderForName(): MutableList<CategoryDB>


    @Query("SELECT * from CATEGORY WHERE id= :id")
    fun getCategoryById(id: String): LiveData<CategoryDB>


    @Query("SELECT name from CATEGORY WHERE id= :id")
    fun getNameCategoryById(id: String): String
}