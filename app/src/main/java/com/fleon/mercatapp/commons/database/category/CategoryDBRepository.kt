package com.fleon.mercatapp.commons.database.category

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.fleon.mercatapp.commons.database.MercatDatabase
import com.fleon.mercatapp.commons.utils.Constant


class CategoryDBRepository(application: Application) {
    private val categoryDao: CategoryDAO? = MercatDatabase.getInstance(application)?.categoryDao()

    //Select all
    fun getAllCategorys(): MutableList<CategoryDB> {
        val sql = "SELECT * FROM CATEGORY ORDER BY name"
        Log.v(Constant.QUERY_ROOM, sql)
        return categoryDao?.getListCategoryOrderForName()!!
    }

    //Get Category for ID
    fun getCategoryById(id: String): LiveData<CategoryDB> {
        val sql = "SELECT * from CATEGORY WHERE id= $id"
        Log.v(Constant.QUERY_ROOM, sql)
        return categoryDao?.getCategoryById(id)!!
    }


    fun getNameCategoryById(id: String): String {
        val sql = "SELECT name from CATEGORY WHERE id= $id"
        Log.v(Constant.QUERY_ROOM, sql)
        return categoryDao?.getNameCategoryById(id)!!
    }

    //Insert
    fun insertCategory(category: CategoryDB){
        if (categoryDao != null) {
            InsertAsyncTask(categoryDao).execute(category)
        }
    }

    private class InsertAsyncTask(private val categoryDao: CategoryDAO) :
        AsyncTask<CategoryDB, Void, Void>() {
        override fun doInBackground(vararg categorys: CategoryDB?): Void? {
            for (category in categorys) {
                if (category != null) categoryDao.insertCategory(category)
            }
            return null
        }
    }

}