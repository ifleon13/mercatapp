package com.fleon.mercatapp.commons.utils

class Constant {

    companion object {
        var KEYID_LIST_CURRENT: String      = "KEYID_LIST_CURRENT"
        var KEY_LIST_ISOPENED: String       = "KEY_LIST_ISOPENED"
        var KEY_NAME_LIST_CURRENT: String   = "KEY_NAME_LIST_CURRENT"

        var KEYID_LIST_FOR_USER_FIREBASE: String  = "KEY_ID_LIST_FOR_USER_FIREBASE"


        //TAG's de la App
        var TAG_APP: String                      = "MercatApp"
        var TAG_DELETE_LIST_ON_FIREBASE: String  = "DELETE_LIST_ON_FIREBASE"
        var TAG_PENDING_IN_FIREBASE: String      = "DELETE_PENDING_IN_FIREBASE"


        //MSN para TAG's
        var MSN_DELETE_TAG_SUCCESS_IN_FIREBASE: String  = "Lista eliminada en Firebase con ID: "
        var MSN_DELETE_TAG_ERROR_IN_FIREBASE: String    = "Error eliminando lista en Firebase con ID: "
        var MSN_DELETE_TAG_PENDING_IN_FIREBASE: String  = "Operación de borrado pendiente ante Firebase"



        var TIME_DELAY_PROCESS_APP: Long = 100
        var NAME_PROCESS_DELEY: String = "Deley"
        var QUERY_ROOM: String = "Query"


        //TAGS PROCESS CORROUTINES
        var TAG_PROCESS_PRODUCT_LIST_SHOP: String  = "PRODUCT_LIST_SHOP"
        var TAG_PROCESS_DELETE_OF_LIST_SHOP: String  = "PRODUCT_LIST_DELETE"
        var TAG_PROCESS_UPDATE_PRODUCT_OF_LIST_SHOP: String  = "PRODUCT_UPDATE"
        var TAG_PROCESS_INSERT_PRODUCT_TO_LIST_SHOP: String  = "PRODUCT_INSERT"
        var TAG_PROCESS_INSERT_LIST: String  = "INSERT_LIST"
        var TAG_PROCESS_INSERT_OR_UPDATE_IN_FIREBASE: String  = "INSERT_OR_UPDATE_IN_FIREBASE"
        var TAG_PROCESS_IS_ENABLE_REOPEN_LIST: String  = "IS_ENABLE_REOPEN_LIST"
        var TAG_PROCESS_LIST_PRODUCT_SHOP: String  = "LIST_PRODUCT_SHOP"
        var TAG_PROCESS_GO_HOME: String  = "GO_HOME"
        var TAG_PROCESS_UPDATE_PRICE_TEMP_PRODUCT_SHOP: String  = "PRICE_TEMP_PRODUCT_SHOP"
        var TAG_PROCESS_GET_FIREBASE_FOR_PRODUCT: String  = "GET_FIREBASE_FOR_PRODUCT"

    }
}