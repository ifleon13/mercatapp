package com.fleon.mercatapp.commons.database.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull


@Entity(tableName = "USER")
data class UserDB(@ColumnInfo(name = "nickname")
                  var nickname: String = "",

                  @ColumnInfo(name = "dateLastLogin")
                  var dateLastLogin: String = "",

                  @ColumnInfo(name = "isLoged")
                  var isLoged: Boolean = false
){

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "userId")
    var userId: Int = 1
}
