package com.fleon.mercatapp.commons.database.list

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "LIST_SHOP")
data class ListDB(@ColumnInfo(name = "namelist")
                  var namelist: String = "",

                  @ColumnInfo(name = "date")
                  var date: String = "",

                  @ColumnInfo(name = "isOpen")
                  var isOpen: Boolean = false,

                  @ColumnInfo(name = "isFavorite")
                  var isFavorite: Boolean = false,

                  @ColumnInfo(name = "value")
                  var value: Float = 0F,

                  @ColumnInfo(name = "countProducts")
                  var countProducts: Int = 0
){

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}