package com.fleon.mercatapp.commons.database.products_list

import com.fleon.mercatapp.commons.database.list.ListDB
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProductListDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProductToList(productListDB: ProductListDB): Long


    @Query("SELECT * FROM PRODUCT_LIST WHERE idList = :idList")
    fun getAllProductOfList(idList: Int): MutableList<ProductListDB>


    @Query("UPDATE PRODUCT_LIST SET price = :price WHERE idProduct = :idProduct AND idList = :idList")
    fun updateAllPriceOfListShopDAO(idList: Int, idProduct: Int, price: Float)


    @Query("SELECT * FROM PRODUCT_LIST WHERE idList = :idList AND pendindSyncFirebase = :status")
    fun getAllProductOfListWithoutSinchronized(idList: Int, status: Boolean): MutableList<ProductListDB>


    @Query("DELETE FROM PRODUCT_LIST WHERE id = :id")
    fun deleteProductOfList(id: Int)


    @Query("UPDATE PRODUCT_LIST SET IdFirebase = :idFirebase WHERE id = :id")
    fun updateIdFirebase(id: Int, idFirebase: String)


    @Query("UPDATE PRODUCT_LIST SET pendindSyncFirebase = :pending WHERE id = :id")
    fun updateStatusPendingSync(id: Int, pending: Boolean)


    @Query("UPDATE PRODUCT_LIST SET pendindSyncFirebase = :pending, cant =:cant, measure =:measure, observ =:obsv  WHERE id = :idProduct AND idList = :idList")
    fun updateDataProduct(idList: Int, idProduct: Int, obsv: String, cant: Int, measure: String, pending: Boolean)

    @Query("UPDATE PRODUCT_LIST SET priceTemp = :priceTemp WHERE idProduct = :idProduct AND idList = :idList")
    fun setPriceTemporalDAO(idList: Int, idProduct: Int, priceTemp: Float)

}