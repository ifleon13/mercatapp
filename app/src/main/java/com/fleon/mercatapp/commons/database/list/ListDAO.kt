package com.fleon.mercatapp.commons.database.list

import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ListDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertList(listDb: ListDB): Long


    @Query("SELECT * FROM LIST_SHOP WHERE isOpen = :isOpen ORDER BY id DESC")
    fun getListOrderForIdWithStatus(isOpen: Boolean): MutableList<ListDB>


    @Query("UPDATE LIST_SHOP SET namelist =:name WHERE id = :id")
    fun updateList(name: String, id: Int)


    @Query("DELETE FROM LIST_SHOP WHERE id = :id")
    fun deleteListForID(id: Int)


    @Query("SELECT * FROM LIST_SHOP WHERE isOpen = :isOpen ORDER BY id DESC LIMIT 1")
    fun getLastListForStatusOpened(isOpen: Boolean): MutableList<ListDB>


    @Query("UPDATE LIST_SHOP SET isFavorite =:isFavorite, value =:value, countProducts =:countProducts, isOpen = :isOpen  WHERE id = :id")
    fun updateListValues(id: Int, isFavorite: Boolean, value: Float, countProducts: Int, isOpen: Boolean)


    @Query("UPDATE LIST_SHOP SET countProducts =:countProducts WHERE id = :idList")
    fun updateCountProductForListDao(countProducts: Int, idList: Int)


    @Query("DELETE FROM LIST_SHOP WHERE id = :id")
    fun deleteList(id: Int)


    @Query("UPDATE LIST_SHOP SET isOpen = :isOpen  WHERE id = :id")
    fun reOpenList(id: Int, isOpen: Boolean)


    @Query("UPDATE LIST_SHOP SET value = :valueTotal  WHERE id = :idList")
    fun updateValueTotalListDAO(idList: Int, valueTotal: Float)
}