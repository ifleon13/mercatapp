package com.fleon.mercatapp.commons.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.fleon.mercatapp.commons.database.category.CategoryDAO
import com.fleon.mercatapp.commons.database.category.CategoryDB
import com.fleon.mercatapp.commons.database.list.ListDAO
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.commons.database.product.ProductDAO
import com.fleon.mercatapp.commons.database.products_list.ProductListDAO
import com.fleon.mercatapp.commons.database.products_list.ProductListDB
import com.fleon.mercatapp.commons.database.user.UserDAO
import com.fleon.mercatapp.commons.database.user.UserDB
import com.fleon.mercatapp.commons.database.product.ProductDB


@Database(entities =
    [UserDB::class,
    CategoryDB::class,
    ProductDB::class,
    ListDB::class,
    ProductListDB::class], version = 7)
abstract class MercatDatabase : RoomDatabase() {

    abstract fun userDao(): UserDAO
    abstract fun categoryDao(): CategoryDAO
    abstract fun listDAO(): ListDAO
    abstract fun productListDAO(): ProductListDAO
    abstract fun productDAO(): ProductDAO

    companion object {
        private const val DATABASE_NAME = "MercatApp.db"
        @Volatile
        private var INSTANCE: MercatDatabase? = null

        fun getInstance(context: Context): MercatDatabase? {
            INSTANCE ?: synchronized(this) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    MercatDatabase::class.java,
                    DATABASE_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return INSTANCE
        }
    }

}