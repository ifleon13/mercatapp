package com.fleon.mercatapp.commons.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

class Jobs {
    companion object JobFactory {

        private val _moviesJob = Job()
        private val coroutineContext: CoroutineContext
            get() = _moviesJob + Dispatchers.Default

        fun get(): CoroutineScope {
            val scope = CoroutineScope(coroutineContext)
            return scope
        }
    }
}