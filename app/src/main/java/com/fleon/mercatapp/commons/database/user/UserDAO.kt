package com.fleon.mercatapp.commons.database.user

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(userDb: UserDB)

    @Query("UPDATE USER SET dateLastLogin=:dateLastLogin WHERE userId = :id")
    fun updateUserWithID(dateLastLogin: String, id: Int)

    @Query("DELETE FROM USER")
    fun deleteAllUser()

    @Query("DELETE FROM USER WHERE userId = :userId")
    fun deleteUserWithId(userId: Int)


    @Query("SELECT * FROM USER ORDER BY dateLastLogin")
    fun getUserOrderDate(): LiveData<UserDB>


    @Query("SELECT * FROM USER WHERE userId = 1")
    fun getLoginUserCurrent(): UserDB?
}