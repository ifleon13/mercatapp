package com.fleon.mercatapp.commons.database.category

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "CATEGORY")
data class CategoryDB(@ColumnInfo(name = "image")
                  var image: String = "",

                  @ColumnInfo(name = "name")
                  var name: String = "",

                  @ColumnInfo(name = "type")
                  var type: String = ""
){

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String = "0"
}