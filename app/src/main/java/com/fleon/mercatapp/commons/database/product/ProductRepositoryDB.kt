package com.fleon.mercatapp.commons.database.product

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import com.fleon.mercatapp.commons.database.MercatDatabase
import com.fleon.mercatapp.commons.database.list.ListDAO
import com.fleon.mercatapp.commons.database.list.ListDB
import com.fleon.mercatapp.commons.database.list.ListRepositoryDB
import com.fleon.mercatapp.commons.utils.Constant


class ProductRepositoryDB(val application: Application) {
    private val productDAO: ProductDAO? = MercatDatabase.getInstance(application)?.productDAO()


    //Inserta un producto
    fun insertProduct(productDB: ProductDB) {
        if (productDAO != null) {
           InsertAsyncTask(productDAO, application).execute(productDB)
        }
    }


    //Obtiene el ID de producto que tiene en Firebase
    fun getIdFirebase(idProduct: Int): String {
        val _sql = "SELECT id FROM PRODUCT WHERE id = :idProduct ORDER BY id DESC LIMIT 1"
        Log.v(Constant.QUERY_ROOM, _sql)
        return productDAO!!.getIDFirebase(idProduct)
    }


    private class InsertAsyncTask(private val productDAO: ProductDAO, val application: Application) :
        AsyncTask<ProductDB, Void, Void>() {
        override fun doInBackground(vararg productDB: ProductDB?): Void? {
            for (newP in productDB) {
                if (newP != null) {
                    var newId = productDAO.insertProduct(newP)

                    if(newId != null){
                        Log.v("AÑADIR", "producto de firebase añadido Exitosamente con ID ")
                    }
                }
            }
            return null
        }
    }
}

