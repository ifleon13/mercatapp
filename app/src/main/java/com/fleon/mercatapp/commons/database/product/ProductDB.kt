package com.fleon.mercatapp.commons.database.product

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "PRODUCT")
data class ProductDB(@ColumnInfo(name = "idProduct")
                      var idProduct: String = "",

                      @ColumnInfo(name = "idCategory")
                      var idCategory: String = "",

                      @ColumnInfo(name = "name")
                      var name: String = "",

                     @ColumnInfo(name = "image")
                     var image: String = "",

                     @ColumnInfo(name = "price")
                     var price: Float = 0F,

                     @ColumnInfo(name = "idChildFirebase")
                     var idChildFirebase: String = "",

                     @ColumnInfo(name = "description")
                     var description: String = ""
){

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String = "0"
}