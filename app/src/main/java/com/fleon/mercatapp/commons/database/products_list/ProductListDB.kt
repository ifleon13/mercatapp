package com.fleon.mercatapp.commons.database.products_list

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "PRODUCT_LIST")
data class ProductListDB(@ColumnInfo(name = "idList")
                  var idList: Int = 0,

                  @ColumnInfo(name = "idProduct")
                  var idProduct: Int = 0,

                  @ColumnInfo(name = "cant")
                  var cant: Int = 0,

                  @ColumnInfo(name = "measure")
                  var measure: String = "",

                  @ColumnInfo(name = "observ")
                  var observ: String = "",

                  @ColumnInfo(name = "name")
                  var name: String = "",

                  @ColumnInfo(name = "price")
                  var price: Float = 0F,

                  @ColumnInfo(name = "idCategory")
                  var idCategory: String = "",

                  @ColumnInfo(name = "nameCategory")
                  var nameCategory: String = "",

                  @ColumnInfo(name = "pendindSyncFirebase")
                  var pendindSyncFirebase: Boolean = true,

                  @ColumnInfo(name = "IdFirebase")
                  var IdFirebase: String = "",

                  @ColumnInfo(name = "priceTemp")
                  var priceTemp: Float = 0F

){

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}