package com.fleon.mercatapp.commons.database.products_list


import android.app.Application
import android.os.AsyncTask
import android.util.Log
import com.fleon.mercatapp.commons.database.MercatDatabase
import com.fleon.mercatapp.commons.utils.Constant


class ProductListRepositoryDB(val application: Application) {
    private val productListDAO: ProductListDAO? = MercatDatabase.getInstance(application)?.productListDAO()


    //Obtiene todos los productos asociados a el ID de la lista
    fun getAllProductOfList(idList: Int): MutableList<ProductListDB> {
        val sql = "SELECT * FROM PRODUCT_LIST WHERE idList = $idList"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.getAllProductOfList(idList)!!
    }


    //Actualiza todos los precios de la lista de compras por los precio compra final pagado
    fun updateAllPriceOfListShopRP(idList: Int, pdtListDB: ProductListDB) {
        val sql = "UPDATE PRODUCT_LIST SET price = ${pdtListDB.price} WHERE idProduct = ${pdtListDB.idProduct} AND idList = $idList"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.updateAllPriceOfListShopDAO(idList, pdtListDB.idProduct, pdtListDB.priceTemp)!!
    }


    //Inserta un nuevo producto a la lista
    fun insertProductToList(productListDB: ProductListDB) {
        if (productListDAO != null) {
            InsertAsyncTask(productListDAO, application).execute(productListDB)
        }
    }


    //Obtiene todos los productos asociados a el ID de lia lista que no han sido sincronizados
    fun getAllProductOfListWithoutSinchronized(idList: Int, status: Boolean): MutableList<ProductListDB> {
        val sql = "SELECT * FROM PRODUCT_LIST WHERE idList = $idList AND pendindSyncFirebase = $status"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.getAllProductOfListWithoutSinchronized(idList, status)!!
    }


    //Elimina un producto
    fun deleteProductToList(idList: Int) {
        val sql = "DELETE FROM PRODUCT_LIST WHERE id = $idList"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.deleteProductOfList(idList)!!
    }


    //Actualiza el campo idFirebase del producto, con el ID asignado en Firebase
    fun updateIDFirebase(id: Int, idFirebase: String) {
        val sql = "UPDATE PRODUCT_LIST SET IdFirebase = $idFirebase WHERE id = $id"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.updateIdFirebase(id, idFirebase)!!
    }


    //Actualiza el estado pendiente de sincronizar ante firebase
    fun updateSyncPending(id: Int, pending: Boolean) {
        val sql = "UPDATE PRODUCT_LIST SET pendindSyncFirebase = $pending WHERE id = $id"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.updateStatusPendingSync(id, pending)!!
    }



    //Actualiza todos los datos de un producto
    fun updateDataProduct(idList: Int, idProduct: Int, obsv: String, cant: Int, measure: String, pending: Boolean) {
        val sql = "UPDATE PRODUCT_LIST SET pendindSyncFirebase = $pending, cant =$cant, measure =$measure, observ =$obsv  WHERE id = $idProduct AND idList = $idList"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.updateDataProduct(idList, idProduct, obsv, cant, measure, pending)!!
    }



    //Asigna el precio temporal asignado en la compra
    fun setPriceTemporalRP(idList: Int, idProduct: Int, priceTemp: Float) {
        val sql = "UPDATE PRODUCT_LIST SET priceTemp = $priceTemp WHERE idProduct = $idProduct AND idList = $idList"
        Log.v(Constant.QUERY_ROOM, sql)
        return productListDAO?.setPriceTemporalDAO(idList, idProduct, priceTemp)!!
    }


    private class InsertAsyncTask(private val productListDAO: ProductListDAO, val application: Application) :
        AsyncTask<ProductListDB, Void, Void>() {
        override fun doInBackground(vararg productListDB: ProductListDB?): Void? {
            for (newProductList in productListDB) {
                if (newProductList != null) {
                    var newId = productListDAO.insertProductToList(newProductList)

                    if(newId != null){
                        Log.v("AÑADIR", "Producto Añadido Exitosamente con ID "+newId.toString() + " a la lista.")
                    }
                }
            }
            return null
        }
    }
}