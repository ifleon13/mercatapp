package com.fleon.mercatapp.commons.database.user

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fleon.mercatapp.commons.database.MercatDatabase
import com.fleon.mercatapp.commons.utils.Constant

class UserDBRepository(application: Application) {
    private val userDao: UserDAO? = MercatDatabase.getInstance(application)?.userDao()

    //Select
    fun getUsers(): LiveData<UserDB> {
        val sql = "SELECT * FROM USER ORDER BY dateLastLogin"
        Log.v(Constant.QUERY_ROOM, sql)
        return userDao?.getUserOrderDate() ?: MutableLiveData()
    }


    fun getLoginUserWithNickName(): UserDB? {
        val sql = "SELECT * FROM USER WHERE userId = 1"
        Log.v(Constant.QUERY_ROOM, sql)
        return userDao?.getLoginUserCurrent()
    }


    //Insert
    fun insertUser(user: UserDB) {
        if (userDao != null) {
            InsertAsyncTask(userDao).execute(user)
        }
    }

    private class InsertAsyncTask(private val userDao: UserDAO) :
        AsyncTask<UserDB, Void, Void>() {
        override fun doInBackground(vararg users: UserDB?): Void? {
            for (user in users) {
                if (user != null) userDao.insertUser(user)
            }
            return null
        }
    }


    //Update
    fun updateUser(user: UserDB, id: Int) {
        if (userDao != null) {
            UpdateAsyncTask(userDao, id).execute(user)
        }
    }

    private class UpdateAsyncTask(private val userDao: UserDAO, val id: Int) :
        AsyncTask<UserDB, Void, Void>() {
        override fun doInBackground(vararg users: UserDB?): Void? {
            for (user in users) {
                if (user != null) {
                    userDao.updateUserWithID(user.dateLastLogin, id)
                }
            }
            return null
        }
    }


    //Delete
    fun deleteUserWithID(id: Int) {
        if (userDao != null) {
            DeleteAsyncTask(userDao, id).execute()
        }
    }

    private class DeleteAsyncTask(private val userDao: UserDAO, val id: Int) :
        AsyncTask<Void, Void, Void>() {
        override
        fun doInBackground(vararg void: Void?): Void? {
            val sql = "DELETE FROM USER WHERE userId = $id"
            Log.v(Constant.QUERY_ROOM, sql)
            userDao.deleteUserWithId(id)
            return null
        }
    }
}